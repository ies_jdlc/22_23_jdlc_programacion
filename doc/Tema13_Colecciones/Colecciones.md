Colecciones
===
Table of concepts:
- [Colecciones](#colecciones)
  - [Introducción](#introducción)
  - [*Collection Framework* y *Collection* interface](#collection-framework-y-collection-interface)
    - [Detalles de las principales colecciones](#detalles-de-las-principales-colecciones)
  - [Cuadro de Interfaces/Clases y sus métodos principales](#cuadro-de-interfacesclases-y-sus-métodos-principales)
    - [Diferencias entre distintas implementaciones de la misma interfaz](#diferencias-entre-distintas-implementaciones-de-la-misma-interfaz)
  - [Comparar elementos para ordenar una lista](#comparar-elementos-para-ordenar-una-lista)
    - [Interfaz *Comparable*](#interfaz-comparable)
    - [Interfaz *Comparator*](#interfaz-comparator)
      - [Tabla Comparable vs Comparator](#tabla-comparable-vs-comparator)


## Introducción
Hasta ahora hemos visto los **tipos primitivos** de datos, que nos han servido para representar caracteres, números enteros, números decimales, valores booleanos,etcétera.  
Después hemos visto **clases de Java** como String, que nos ha permitido manejar un número variable y no predeterminado de caracteres formando palabras, frases y textos tan largos como quisiéramos, pudiendo añadir/concatenar posteriormente más texto.  
También hemos visto cómo crear **nuestras propias clases**, que son estructuras de datos que nos permiten agrupar varias variables de diferentes tipos, ya sean tipos primitivos u otras clases (las clases además nos permiten definir comportamientos a través de sus métodos).  
Con los **Arrays** hemos aprendido a crear conjuntos de tamaño fijo predefinido, ya fuesen de 1, 2 o n dimensiones.  
Las **colecciones** ([tutorial oficial](https://docs.oracle.com/javase/tutorial/collections/TOC.html)) son estructuras de datos que nos permiten almacenar **objetos** sin tener que predefinir el máximo número que vamos a poder gestionar. Las colecciones crecen dinámicamente según añadimos elementos y también pueden reducir su tamaño si borramos elementos de las mismas. Las colecciones son clases de Java y se encuentran en el package *java.util*.  
Una colección representa un grupos de objetos, también llamados elementos. Algunas colecciones permiten elementos duplicados y otras no. Unas mantienen los elementos ordenados por sus valores, mientras que otras preservar el orden de inserción. Algunas son más rápidas añadiendo elementos en una posición determinada, mientras que otras son más rápidas para encontrar si un valor existe en la colección.

## *Collection Framework* y *Collection* interface
El término *Collection* en Java pueden hacer referencia a dos conceptos muy relacionados pero diferentes: [Collection Framework](https://docs.oracle.com/javase/8/docs/technotes/guides/collections/overview.html) y la interfaz [Collection](https://docs.oracle.com/javase/tutorial/collections/interfaces/collection.html). Además de la interfaz Collection, dentro de *Collection Framework* está también definida la interfaz [Map](https://docs.oracle.com/javase/tutorial/collections/interfaces/map.html), que veremos más adelante.
Partiendo de la [interfaz](https://docs.oracle.com/javase/tutorial/collections/interfaces/index.html) [*Collection*](https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html) podemos encontrar otras subinterfaces hasta que finalmente lleguemos a una clase que tenga implementados todos los métodos abstractos definidos en la cadena de herencia. En algunos casos puede haber clases abstractas intermedias que implementen algunos métodos abstractos definidos en las interfaces. La interfaz *Collection* es una subinterfaz de [*Iterable*](https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html), la cual declara el método abstrato [*iterator*](https://docs.oracle.com/javase/8/docs/api/java/lang/Iterable.html#iterator--) para implementar este patrón de diseño que nos permite recorrer todos los elementos de una colección, sea cual sea.  
Ninguna clase implementa directamente la interfaz *Collection*, sino que son subinterfaces de esta, como List o Set, las que se acaban implementando con clases como, por ejemplo, *ArrayList* o *TreeSet*.


![Herencia de Collection y List](img/herenciaiterable_arquitecturajava.com.png)  
*Imagen obtenida de https://www.arquitecturajava.com/java-collections-framework-y-su-estructura/

Todas las clases de Java que implementan la interfaz *Collection* ofrecen al menos 2 constructores: uno vacío que crea una colección sin ningún elemento y otro constructor que recibe otra clase que también implementa la interfaz *Collection* y que inicializa la nueva colección insertando todos los elementos contenidos en la colección recibida como argumento del constructor.

Por ejemplo, el siguiente código: 
```java
List<String> listaCadenas = new ArrayList<>();
System.out.println(">> listaCadenas "+ (listaCadenas.isEmpty()?"está vacía":"NO está vacía"));
listaCadenas.add("Hola");
listaCadenas.add("Adios");

System.out.println("listaCadenas:");
for(String cadena: listaCadenas){
  System.out.println(cadena);
}

Set<String> conjuntoCadenas = new TreeSet<>(StacklistaCadenas);
System.out.println(">> conjuntoCadenas "+ (conjuntoCadenas.isEmpty()?"está vacío":"NO está vacío"));

System.out.println("conjuntoCadenas:");
for(String cadena: conjuntoCadenas){
  System.out.println(cadena);
}

```
Produce la siguiente salida:
```
>> listaCadenas está vacía
listaCadenas:
Hola
Adios
>> conjuntoCadenas no está vacío
conjuntoCadenas:
Adios
Hola
```
*Atención: Nótese cómo un TreeSet ordena los elementos por su valor. En este caso, al ser Strings, las ordena alfabéticamente*

### Detalles de las principales colecciones

* [Collection](https://docs.oracle.com/javase/tutorial/collections/interfaces/collection.html): La interfaz *Collection* define los métodos abstractos que toda coleción debe implementar. Entre estos métodos están *add(e)*, *clear()*, *contains(o)*, *isEmpty()*, *remove(o)*, *size()*.

* [List](https://docs.oracle.com/javase/tutorial/collections/interfaces/list.html): Una lista es una colección de elementos ordenada, también llamada secuencia. Permite controla dónde se inserta cada elemento dentro de la lista. Los elementos de una lista pueden ser accedidos por su posición. A diferencia de los *Set* (conjunto), las listas permiten elementos duplicados.
* [Set](https://docs.oracle.com/javase/tutorial/collections/interfaces/set.html): Un conjunto es una colección que no admite duplicados. Es decir, no pueden existir dos elemenentos *e1* y *e2* tales que *e1.equals(e2)* sea true. La comparación con el método *equals* hace una comparación al nivel de referencias o lo que sería equivalente a direcciones de memoria. Por lo tanto, dos objetos diferentes, construidos pasándoles idénticos parámetros al constructor, no se considerarán iguales por el método *equals*.  
  La sobreescritura del método *equals* generalmente requiere también la sobreescritura del método *hashCode*, lo cual va más allá del contenido de este curso.
* [Queue](https://docs.oracle.com/javase/tutorial/collections/interfaces/queue.html): Una cola es una colección diseñada para almacenar elementos antes de su procesamiento, ya que conserva un orden determinado. El orden más frecuente sigue el estilo first-in-first-out (FIFO), pero hay otras posibilidades, como la PriorityQueue (como podría ser el [triaje](https://es.wikipedia.org/wiki/Triaje) en las urgencias de un hospital). Las colas proporcionan operaciones adicionales para insertar, extraer y consultar el siguiente elemento. Existen 2 grupos de métodos para cada una de estas 3 operaciones: los que en caso de fallo lanzan una excepción y los que en caso de fallo devuelven un valor especial (*false* o *null* normalmente).
* [Stack](https://docs.oracle.com/javase/8/docs/api/java/util/Stack.html): Una pila es una colección cuyos elementos siguen el estilo last-in-first-out (LIFO). Sus operaciones principales son *push* y *pop*. Es una clase que hereda de la clase *Vector*.
* [Map](https://docs.oracle.com/javase/tutorial/collections/interfaces/map.html): Un mapa es una colección que "mapea" claves a valores. Es decir, utiliza el valor de un objeto como la clave para acceder a otro objeto. Un mapa no puede tener claves duplicadas, por lo que una clave sólo podrá reverenciar un valor. Los principales métodos que ofrecen los mapas son añadir pares clave-valor (*put*), buscar una clave y obtener su valor asociado (*get*), eliminar un par clave-valor (*remove*), comprobar si una clave está presente en el mapa (*containsKey*) y comprobar si un valor asociado está presente en el mapa (*containsValue*).  
  Como se ha comentado anteriormente, *Map* no hereda de la interfaz *Collection*, pero sí pertenece al *Collection Framework* de Java.

## Cuadro de Interfaces/Clases y sus métodos principales

| Interfaz/Clase | Métodos                                                                            |
| -------------- | ---------------------------------------------------------------------------------- |
| Iterable       | *iterator()*                                                                         |
| Collection     | *add(e), clear(), contains(o), isEmpty(), remove(o), size()*                       |
| List           | *add(index, e), set(index), get(index), remove(index), indexOf(o), lastIndexOf(o)* |
| Queue          | *add(e)/offer(e), remove()/poll(), element()/peek()*                               |
| Stack          | *push(e), pop(), peek()*                                                           |
| Map            | *put(k,v), get(k), remove(k), containsKey(k), containsValue(v)*                    |

### Diferencias entre distintas implementaciones de la misma interfaz
Como quizá ya se haya apreciado al revisar ejemplos de uso de colecciones, no hay una implementación única para cada una de estas interfaces. Hay múltipleas factores que afectan a la hora de decidir cómo implementar una colección y qué implementación elegir en cada caso.

En la sección *Collection Implementations* de [Collections Framework Overview](https://docs.oracle.com/javase/8/docs/technotes/guides/collections/overview.html) se indica que los nombres de las clases que implementan interfaces del *Collection Framework* suelen seguir la nomenclatura *\<Estilo-implementación\>\<Interfaz\>*:

| Interface | [Hash Table](https://en.wikipedia.org/wiki/Hash_table) | Resizable Array | [Balanced Tree](https://en.wikipedia.org/wiki/Self-balancing_binary_search_tree) | [Linked List](https://en.wikipedia.org/wiki/Linked_list) | Hash Table + Linked List |
| --------- | ------------------------------------------------------ | --------------- | -------------------------------------------------------------------------------- | -------------------------------------------------------- | ------------------------ |
| *Set*     | HashSet                                                |                 | TreeSet                                                                          |                                                          | LinkedHashSet            |
| *List*    |                                                        | ArrayList       |                                                                                  | LinkedList                                               |                          |
| *Deque*   |                                                        | ArrayDeque      |                                                                                  | LinkedList                                               |                          |
| *Map*     | HashMap                                                |                 | TreeMap                                                                          |                                                          | LinkedHashMap            |

![Jerarquía de Colecciones en Java](img/java-collection-hierarchy_www.scientecheasy.com.png)  
*Imagen obtenida de https://www.scientecheasy.com/2020/09/collection-hierarchy-in-java.html


A modo de resumen breve, vamos a enumerar los conceptos nuevos citados en la tabla anterior:
* Hash Table: estructura de datos que almacena sus elementos en función a un valor (hash code) obtenido de una operación matemática llamada función hash. 
Esta función suele ser muy rápida, por lo que estas tablas permiten acceder rápidamente a la posición en la que se encuentra un elemento. Como
inconveniente tenemos que perdemos el orden de inserción y las tablas hash no mantienen ningún orden para sus elementos.
* Balanced Tree: estructura de datos en forma de árbol que se mantiene siempre ordenado gracias a sus operaciones de *rebalanceo* en cada inserción o eliminación.
No es tan rápido como una tabla hash, pero es lo más eficiente si queremos mantener un conjunto de elementos ordenados.
* Linked List: estructura de datos cuyos elementos se almacenan en nodos que tienen referencias al siguiente nodo (y también al anterior en muchos casos). La ventaja es que podemos mantener una lista en un orden determinado y añadir un elemento nuevo en la posición deseada de forma eficiente, ya que no requiere mover el resto de los elementos, sólo hay que cambiar la referencia *next* del elemento anterior y la referencia *previous" del elemento siguiente. Como inconveniente, no tenemos acceso aleatorio a sus elementos a partir de su posición o de su valor. Debemos realizar acceso secuencial, ya que el objeto de tipo linked list suele tener únicamente la referencia al primer elemento (y también al último en muchos casos).



Si combinamos la tabla y con el siguiente diagrama:  
![Diagrama elección colección](img/diagrama_java-collections-img1_adictosaltrabajo.com.png)
*Imagen obtenida de https://www.adictosaltrabajo.com/2015/09/25/introduccion-a-colecciones-en-java/

Podemos sacar las siguientes conclusiones:

1. En el diagrama se representa el diagrama de decisión entre los distintos tipos de Map, List y Set.
2. Los Map y los Set no permiten duplicados, ArrayList sí los permite. (*Stack* permite duplicados porque hereda de Vector, que implementa *List*. *Queue* también acepta duplicados en principio, aunque alguna implementación podría restringirlo)
3. Las colecciones que implementan List (secuencia de objetos) no son eficientes buscando elementos, ya sea para comprobar si lo contiene o para eliminarlos.
4. Si queremos poder almacenar pares de clave-valor, tenemos que optar por las colecciones que implementan la interfaz *Map*. El resto de colecciones implementa la interfaz *Collection* y almacena objetos sin estar asociados a una clave.
5. Si el orden no es importante, pero queremos poder buscar elementos de forma eficiente, debemos optar por las implementaciones que utilicen tablas Hash.
6. Si queremos mantener la colección ordenada por orden de inserción, debemos optar por colecciones *Linked* (supongo que utilizarán una linked list internamente).
7. Si queremos mantener la colección ordenada por el valor de sus elementos, debemos optar por colecciones *Tree*.


## Comparar elementos para ordenar una lista
> **AVISO**: Las clases que implementan la interfaz *Set* (conjunto) utilizan internamente el método *equals* para comprobar si un elemento ya existe y en ese caso no añadirlo, ya que los conjuntos no permiten elementos duplicados. El método de comparación de elementos debe ser consistente con el método *equals*. Sobreescribir el método *equals* requiere sobreescribir el método *hashCode*. Todos estas dependencias pueden tener implicaciones que se escapen al contenido de esta asignatura. Se puede (y se debe) jugar a hacer cambios y ver qué pasa, pero para practicar la comparación de objetos y la ordenación de colecciones se recomienda ceñirse a las clases que hereden de la interfaz *List* y de la interfaz *Map*.
> Más detalles [aquí](https://stackoverflow.com/questions/27581/what-issues-should-be-considered-when-overriding-equals-and-hashcode-in-java/27609#27609), [aquí](https://www.geeksforgeeks.org/overriding-equals-method-in-java/) y [aquí](http://www.technofundo.com/tech/java/equalhash.html)

[Ordenación de objetos. Tutorial oficial de Java](https://docs.oracle.com/javase/tutorial/collections/interfaces/order.html)

Para que podamos ordenar una colección de elementos, debemos ser capaces de comparar un elemento con otro y poder decir si el primero es menor, igual o mayor que el segundo. Ordenar una lista de números es algo natural que no necesitamos pararnos a pensar. Ordenar cadenas de caracteres alfabéticamente también, aunque habría que tener claro si los dígitos del 0 al 9 van antes o después que los caracteres alfabéticos, y si las mayúsculas van antes o después que las minúsculas. En programación, a este orden se le llama *natural ordering* (orden natural). Java tiene claro cómo ordenar números y Strings. Los elementos que se añadan a una estructura de tipo Tree, se irán insertando de forma que queden ordenados por su orden natural. Si tenemos una lista de Strings, por ejemplo un *ArrayList\<String\>*, cuyo orden por defecto es el de inserción, podremos pedirle que nos las reordene un usando el método `Collections.sort(List)` con un código como el siguiente:

```java
    // Creamos una lista de Strings vacía
    List<String> listaCadenas = new ArrayList<>();
    
    // Añadimos cadenas a la lista
    listaCadenas.add("Hola");
    listaCadenas.add("Buenos dias");
    listaCadenas.add("Buenos tardes");
    listaCadenas.add("Buenos noches");
    listaCadenas.add("Adios");

    // Imprimimos la lista
    System.out.println(">> listaCadenas =>");
    for (String cadena : listaCadenas) {
      System.out.println("\t" + cadena);
    }

    // Llamamos al método Collections.sort(listaCadenas) para ordenar las cadenas de la lista 
    Collections.sort(listaCadenas);
    System.out.println(">> listaCadenas = Collections.sort(listaCadenas) =>");
    for (String cadena : listaCadenas) {
      System.out.println("\t" + cadena);
    }
```

Produce la siguiente salida:

```
>> listaCadenas =>
        Hola
        Buenos dias
        Buenos tardes
        Buenos noches
        Adios
>> listaCadenas = Collections.sort(listaCadenas) =>
        Adios
        Buenos dias
        Buenos noches
        Buenos tardes
        Hola
```

### Interfaz *Comparable*
Pero cuando queremos ordenar una colección cuyos elementos son productos frescos de una empresa agroalimentaria o clientes de una empresa de alquiler de vehículos, ya no está tan claro cuál es menor y cuál es mayor. Debemos decidir qué criterio aplicar para comparar los objetos del mismo tipo. Los productos los podríamos ordenar alfabéticamente por su nombre, por su fecha de envasado, por su número de lote o por su fecha de caducidad. E incluso por combinaciones de las anteriores. 

Para definir el orden natural de los objetos de una clase, en Java debemos implementar la interfaz [*Comparable*](https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html), que sólo nos exige implementar el método abstracto [*compareTo*](https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html#compareTo-T-).

Imaginemos que tenemos la siguiente clase *Persona*:
```java
public class Persona {
  String nombre;
  String dni;
  LocalDate fechaNacimiento;

  public Persona(String nombre, String dni, String fechaNacimiento) {
    this.nombre = nombre;
    this.dni = dni;
    this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
  }

  @Override
  public String toString() {
    return String.format("%10s, %9s, %s", nombre, dni, fechaNacimiento);
  }
}

```
Y el siguiente método main:
```java
  public static void main(String[] args) {
    List<Persona> listaPersonas = new ArrayList<>();
    listaPersonas.add(new Persona("Mario", "12345678A", "1925-05-01"));
    listaPersonas.add(new Persona("Sara", "52345678F", "1985-05-01"));
    listaPersonas.add(new Persona("Maria", "22345678B", "1950-10-21"));
    listaPersonas.add(new Persona("Ana", "92345678J", "1990-10-21"));

    Collections.sort(listaPersonas);
    System.out.println(">> Collections.sort(listaPersonas):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }
```

Hay un error en la línea `Collections.sort(listaPersonas)` porque el compilador no sabe comparar objetos de tipo *Persona*. Pero si hacemos que la clase *Pesona* implemente la interfaz *Comparable* e implementamos su método *compareTo* para que compare personas basándose, por ejemplo, en su nombre (nombre es de tipo String, y esta clase ya tiene método *compareTo*, así que lo utilizamos):
```java
public class Persona implements Comparable<Persona> {
  
  [...]
  
  @Override
  public int compareTo(Persona persona) {
    return nombre.compareTo(persona.nombre);
  }
}
```

El método *main* anterior ya funciona y produce la siguiente salida:
```
>> Collections.sort(listaPersonas):
     Ana, 92345678J, 1990-10-21
   Maria, 22345678B, 1950-10-21
   Mario, 12345678A, 1925-05-01
    Sara, 52345678F, 1985-05-01
```

Pero, ¿qué ocurriría si quisiésemos ordenar una lista de Personas por nombre y después ordenarla por fecha de nacimiento para acceder rápidamente, por ejemplo, a los más mayores o los más jóvenes? ¿y qué ocurriría si quisiéramos ordenar una lista de objetos de una clase que no hemos desarrollado nosotros y que, o no implementa la interfaz *Comparable*, o queremos ordenarla por otro criterio diferente a su orden natural?  
Necesitamos la interfaz *Comparator*.

### Interfaz *Comparator*

La interfaz *Comparator* nos permite definir clases cuya función es comparar 2 objetos de una clase y determinar si el primero es menor, igual o mayor que el segundo. Para hacer esto, el comparador tiene que implementan su método abstracto *compare*. Una vez tengamos una clase que implemente esta interfaz para comparar nuestros objetos, se lo podremos pasar como segundo parámetro al método [`Collections.sort(List, Comparator)`](https://docs.oracle.com/javase/8/docs/api/java/util/Collections.html#sort-java.util.List-java.util.Comparator-).

Siguiendo con el ejemplo de Persona, yo podría crearme la siguiente clase [*PersonaComparatorFechaNacimiento.java*](src/PersonaComparatorFechaNacimiento.java), que implementa la interfaz `Comparator<Persona>`:
```java
import java.util.Comparator;

public class PersonaComparatorFechaNacimiento implements Comparator<Persona> {
  @Override
  public int compare(Persona o1, Persona o2) {
    if (o1.fechaNacimiento.isBefore(o2.fechaNacimiento)) {
      return -1;
    }
    if (o1.fechaNacimiento.isAfter(o2.fechaNacimiento)) {
      return 1;
    }
    return 0;
  }
}
```

Y añadiendo este código al final del método main, crearía un objeto de tipo *PersonaComparatorFechaNacimiento* y lo utilizaría como comparador la ordenar la lista de personas:
```java
  public static void main(String[] args) {
  
    [...]
    
    PersonaComparatorFechaNacimiento comparatorPersonasFechaNacimiento = new PersonaComparatorFechaNacimiento();
    Collections.sort(listaPersonas, comparatorPersonasFechaNacimiento);
    System.out.println(">> Collections.sort(listaPersonas, comparatorPersonasFechaNacimiento):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }
```

Que produciría la siguiente salida:
```
>> Collections.sort(listaPersonas, comparatorPersonasFechaNacimiento):
   Mario, 12345678A, 1925-05-01
   Maria, 22345678B, 1950-10-21
    Sara, 52345678F, 1985-05-01
     Ana, 92345678J, 1990-10-21
```

Con la interfaz `Comparable<Persona>` definíamos el orden natural para los objetos de la clase *Persona*, pero el orden natural es único para cada clase: una vez que implementamos el criterio de orden natural ya no lo podemos cambiar. En cambio, gracias a la creación de *comparators* (usando la interfaz `Comparator<Persona>`) podemos ordenar colecciones de personas por diferentes criterios, creando un comparador para cada criterio.

Crear una instancia del objeto comparator cada vez que se vaya a ordenar produce un código nada elegante, por lo que los comparators para los diferentes criterios suelen crearse como atributos *public static final* en la clase del objeto a ordenar. Además, siguen una sintaxis que no habíamos visto hasta ahora en la que, después de llamar al constructor con new, en vez de punto y coma después de cerrar los paréntesis, se abren las llaves para definir la clase que implementa el comparador.

Ejemplo:
```java
  public static final Comparator<Persona> DNI_COMPARATOR = new Comparator<Persona>() {
    public int compare(Persona p1, Persona p2) {
      return p1.dni.compareTo(p2.dni);
    }
  };
```
Estamos creando un objeto de una clase que implementa la interfaz  `Comparator<Persona>`. Como esta clase sólo la vamos a utilizar para crear este comparador una única vez, no hace falta darle nombre a la clase y escribirla en un fichero independiente. Es una sintaxis nueva, similar a la de las inner/nested class (clase anidada).

**NOTA: Se recomienda revisar los ejemplos de Comparator en el fichero Persona.java, enlazados en el siguiente párrafo:**  
 En nuestro caso, es en el fichero [Persona.java](src/Persona.java) donde hemos creado los  *comparator*s [DNI_COMPARATOR](src/Persona.java#L11) y [FECHA_NACIMIENTO_COMPARATOR](src/Persona.java#L17), dejando el método [main](src/MainPersonasComparableComparators.java) libre de andar construyendo los *comparators*. 

#### Tabla Comparable vs Comparator

|Interfaz|Método|Descripción|
|-|-|-|
|Comparable\<C\> | int compareTo(C c) | Establece el orden natural de los objetos de tipo C. Criterio único. La clase C implementa la interfaz Comparable\<C\>.|
| Comparator\<C\> | int compare(C c1, C c2) | Define un comparador por un criterio. Se pueden crear tantos comparadores como haga falta.<br> No es la clase C la que implementa la interfaz *Comparator*, sino otra clase que hará de comparador.|