import java.time.LocalDate;
import java.util.Comparator;

public class Persona implements Comparable<Persona> {
  // ATRIBUTOS
  String nombre;
  String dni;
  LocalDate fechaNacimiento;
  
  // Comparators (atributos public static final):
  public static final Comparator<Persona> DNI_COMPARATOR = new Comparator<Persona>() {
    public int compare(Persona p1, Persona p2) {
      return p1.dni.compareTo(p2.dni);
    }
  };

  public static final Comparator<Persona> FECHA_NACIMIENTO_COMPARATOR = new Comparator<Persona>() {
    public int compare(Persona p1, Persona p2) {
      return p1.fechaNacimiento.compareTo(p2.fechaNacimiento);
    }
  };

  // MÉTODOS
  public Persona(String nombre, String dni, String fechaNacimiento) {
    this.nombre = nombre;
    this.dni = dni;
    this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
  }

  @Override
  public String toString() {
    return String.format("%-10s %9s %s", nombre, dni, fechaNacimiento);
  }

  @Override
  public int compareTo(Persona persona) {
    return nombre.compareTo(persona.nombre);
  }
}
