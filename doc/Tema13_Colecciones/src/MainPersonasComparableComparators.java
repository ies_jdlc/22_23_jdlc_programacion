import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainPersonasComparableComparators {
  public static void main(String[] args) {
    List<Persona> listaPersonas = new ArrayList<>();
    listaPersonas.add(new Persona("Mario", "12345678A", "1925-05-01"));
    listaPersonas.add(new Persona("Sara", "52345678F", "1985-05-01"));
    listaPersonas.add(new Persona("Maria", "22345678B", "1950-10-21"));
    listaPersonas.add(new Persona("Ana", "92345678J", "1990-10-21"));

    System.out.println(">> listaPersonas:");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }

    Collections.sort(listaPersonas);
    System.out.println(">> Collections.sort(listaPersonas):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }

    Collections.sort(listaPersonas, Comparator.reverseOrder());
    System.out.println(">> Collections.sort(listaPersonas, Comparator.reverseOrder()):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }

    Collections.sort(listaPersonas, Persona.DNI_COMPARATOR);
    System.out.println(">> Collections.sort(listaPersonas, Persona.DNI_COMPARATOR):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }

    Collections.sort(listaPersonas, Persona.FECHA_NACIMIENTO_COMPARATOR);
    System.out.println(">> Collections.sort(listaPersonas, Persona.FECHA_NACIMIENTO_COMPARATOR):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }

    Collections.sort(listaPersonas, Persona.DNI_COMPARATOR.reversed());
    System.out.println(">> Collections.sort(listaPersonas, Persona.DNI_COMPARATOR.reversed()):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }

    Collections.sort(listaPersonas, Persona.FECHA_NACIMIENTO_COMPARATOR.reversed());
    System.out.println(">> Collections.sort(listaPersonas, Persona.FECHA_NACIMIENTO_COMPARATOR.reversed()):");
    for (Persona persona : listaPersonas) {
      System.out.println("\t" + persona);
    }
  }
}
