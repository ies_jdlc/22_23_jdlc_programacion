import java.util.Comparator;

public class PersonaComparatorFechaNacimiento implements Comparator<Persona> {
  @Override
  public int compare(Persona o1, Persona o2) {
    if (o1.fechaNacimiento.isBefore(o2.fechaNacimiento)) {
      return -1;
    }
    if (o1.fechaNacimiento.isAfter(o2.fechaNacimiento)) {
      return 1;
    }
    return 0;
  }
}
