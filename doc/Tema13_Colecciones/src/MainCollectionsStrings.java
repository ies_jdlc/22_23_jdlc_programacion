import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainCollectionsStrings {
  public static void main(String[] args) {

    // Creamos una lista de Strings vacía
    List<String> listaCadenas = new ArrayList<>();
    System.out.println(">> listaCadenas " + (listaCadenas.isEmpty() ? "está vacía" : "no está vacía"));
    
    // Añadimos cadenas a la lista
    listaCadenas.add("Hola");
    listaCadenas.add("Buenos dias");
    listaCadenas.add("Buenos tardes");
    listaCadenas.add("Buenos noches");
    listaCadenas.add("Adios");

    // Imprimimos la lista
    System.out.println(">> listaCadenas =>");
    for (String cadena : listaCadenas) {
      System.out.println("\t" + cadena);
    }

    // Creamos un conjunto de Strings y lo inicializamos con la lista de cadenas que hemos ido rellenando.
    // Como estamos usando un TreeSet, los elementos de la colección se ordenan por su valor
    Set<String> conjuntoCadenas = new TreeSet<>(listaCadenas);
    System.out.println(">> conjuntoCadenas " + (conjuntoCadenas.isEmpty() ? "está vacío" : "no está vacío"));

    System.out.println(">> conjuntoCadenas =>");
    for (String cadena : conjuntoCadenas) {
      System.out.println("\t" + cadena);
    }

    // Llamamos al método Collections.sort(listaCadenas) para ordenar las cadenas de la lista 
    Collections.sort(listaCadenas);
    System.out.println(">> listaCadenas = Collections.sort(listaCadenas) =>");
    for (String cadena : listaCadenas) {
      System.out.println("\t" + cadena);
    }

    // Llamamos al método Collections.sort(listaCadenas, Comparator.reverseOrder()) para ordenar 
    // las cadenas de la lista en orden inverso 
    Collections.sort(listaCadenas, Comparator.reverseOrder());
    System.out.println(">> listaCadenas = Collections.sort(listaCadenas, Comparator.reverseOrder()) =>");
    for (String cadena : listaCadenas) {
      System.out.println("\t" + cadena);
    }

  }
}
