Excepciones
===
Table of concepts:
- [Excepciones](#excepciones)
  - [Introducción](#introducción)
  - [Bloques de código para gestión de excepciones](#bloques-de-código-para-gestión-de-excepciones)
    - [Bloques *try*, *catch* y *finally*](#bloques-try-catch-y-finally)
    - [Bloque especial *try-with-resources*](#bloque-especial-try-with-resources)
    - [Ejemplos extendidos de *try-catch-finally* y de *try-with-resources*](#ejemplos-extendidos-de-try-catch-finally-y-de-try-with-resources)
  - [Orden de los bloques *catch* que gestionan/manejan una excepción](#orden-de-los-bloques-catch-que-gestionanmanejan-una-excepción)
    - [Ejemplo de diferentes bloques *catch* en el orden correcto](#ejemplo-de-diferentes-bloques-catch-en-el-orden-correcto)
  - [Excepciones de tipo checked/unchecked (comprobadas/no-comprobadas)](#excepciones-de-tipo-checkedunchecked-comprobadasno-comprobadas)
  - [*throw* y *throws*](#throw-y-throws)
  - [Crear nuestras propias excepciones](#crear-nuestras-propias-excepciones)


## Introducción
*Exception* es una abreviación de *exceptional event* (evento excepcional).  
[Tutorial oficial Excepciones en Java](https://docs.oracle.com/javase/tutorial/essential/exceptions/index.html)

Una excepción es un evento que ocurre durante la ejecución de un programa y que interrumple el flujo normal de ejecución.  
Cuando, durante la ejecución de un método se produce una excepción, se crea un tipo especial de objeto y se pasa al *runtime system* (sistema de ejecución de la máquina virtual de Java). Este objeto, también llamado excepción, contiene información sobre el error, como el tipo y el estado del programa cuando se produjo la excepción.

Crear una excepción y pasársela al *runtime system* se llama *lanzar una excepción*. Una vez un método lanza una excepción, el *runtime system* tiene que encontrar el código encargado de manejar esa excepción. La búsqueda del código que maneja la excepción se realiza en el método donde se lanza la excepción, y de no encontrarse el *handler* (manejador o controlador) de la excepción, sigue buscando en los métodos que se encuentren en el *call stack* (pila de llamadas) en el orden inverso al que fueron ejecutados. Es decir, si una excepción se lanza en un método y no se captura en ese mismo método, se buscará el *handler* en el método desde el que se había llamado al método inicial. Si tampoco se encuentra en ese segundo método, se seguirá buscando en la pila de llamadas hasta el método *main*. Si en el *main* tampoco se encuentra un manejador para la excepción, se termina la ejecución del programa.

Para considerarse que un manejador de excepción es adecuado para manejar una excepción de un tipo determinado, el tipo de excepción lanzada deberá coincidir con el tipo de excepción capturada o manejada (se aplican las reglas de herencia y polimorfismo).

##  Bloques de código para gestión de excepciones
### Bloques *try*, *catch* y *finally*
En java, para manejar las excepciones se utilizan unos bloques de código específicos llamados:
1. [*try*](https://docs.oracle.com/javase/tutorial/essential/exceptions/try.html)  
   El código dentro de un bloque *try* se ejecutará y, en el caso de que lance una excepción, se intentará capturar en el bloque *catch*.
2. [*catch(ExceptionType exceptionReference)*](https://docs.oracle.com/javase/tutorial/essential/exceptions/catch.html)  
   El código dentro del bloque *catch* se ejecutará si en el bloque *try* previo se ha lanzado una excepción del tipo especificado por el *catch* entre paréntesis, *ExceptionType* en este ejemplo. En el caso de haber capturado una excepción de tipo *ExceptionType*, podremos referirnos a ella para invocar sus métodos o para volver a lanzarla, mediante el identificador de la referencia, *exceptionReference* en el ejemplo.
3. [*finally*](https://docs.oracle.com/javase/tutorial/essential/exceptions/finally.html)  
   El bloque de código del *finally* se ejecuta siempre, se haya lanzado o no una excepción en el bloque *try*.

Estructura de bloques *try-catch-finally*:
```java
try {
  //  Bloque de código que podría lanzar una excepción
}
catch(ExceptionType exceptionReference) {
  //  Bloque de código que controlar la excepción si se lanza y es de tipo ExceptionType
}
finally {
  // Bloque de código que se ejecuta siempre, se lance o no una excepción
}
```
### Bloque especial [*try-with-resources*](https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html)
Hay un tipo de bloque *try* que inicializa unos recursos y se encarga de liberarlos independientemente de lo que ocurra con las excepciones y su gestión. Esto es especialmente útil cuando se trabaja con ficheros. Los recursos que funcionan en este tipo especial de bloque *try* son aquellos objetos que implementen la interfaz [*java.lang.AutoCloseable*](https://docs.oracle.com/javase/8/docs/api/java/lang/AutoCloseable.html), que incluye la interfaz [*java.io.Closeable*](https://docs.oracle.com/javase/8/docs/api/java/io/Closeable.html) de la que heredan todas clases que hemos visto para la lectura y escritura de ficheros.

Partiendo del siguiente código que cierra el recurso `escritor` en el bloque *finally*:
```java
FileWriter escritor = null;
try {
  escritor = new FileWriter(ProductoFresco.CSV_NOMBRE_FICHERO);
  escritor.write(ProductoFresco.csvHeader() + "\n");
  for (ProductoFresco fresco : listaFrescos) {
    escritor.write(fresco.toCsvLine() + "\n");
  }
} catch (Exception e) {
  System.out.println("Excepción capturada en guardarCsv: " + e);
} finally {
  if (escritor != null) {
    try{
      escritor.close();
    } catch (IOException e) {
      System.out.println("IOException capturada al ejecutar escritor.close(): " + e);
    }
  }
}
```
Podríamos reescribirlo usando un bloque *try-with-resources* de la siguiente manera:

```java
try (FileWriter escritor = new FileWriter(ProductoFresco.CSV_NOMBRE_FICHERO)) {
  escritor.write(ProductoFresco.csvHeader() + "\n");
  for (ProductoFresco fresco : listaFrescos) {
    escritor.write(fresco.toCsvLine() + "\n");
  }
} catch (Exception e) {
  System.out.println("Excepción capturada en guardarCsv: " + e);
}
```

Al utilizar un bloque *try-with-resources* en vez de un *try* de los que habíamos visto hasta ahora, los recursos se liberarán automáticamente. En este caso, se encargará de llamar a `escritor.close()`.


### Ejemplos extendidos de *try-catch-finally* y de *try-with-resources*
* Ejemplo más amplio con el uso de *try*, *catch*, *finally* y *throw* [aquí](src/MainExcepcionesConTryCatchFinallyThrow.java).
* Ejemplo de comparación de bloque *try-catch-finally* normal y otro que usa  *try-with-resources*, *catch* y *finally* [aquí](src/MainExcepcionesConTryWithResources.java). En este ejemplo se ha implementado dos funciones que cuentan el número de líneas que tiene un fichero: [una versión](src/MainExcepcionesConTryWithResources.java#L32) usa el bloque *try* normal y la [otra versión](src/MainExcepcionesConTryWithResources.java#L82) usa *try-with-resources*.

## Orden de los bloques *catch* que gestionan/manejan una excepción
Después de un bloque *try* puede haber varios bloques *catch*, teniendo cada uno de ellos un tipo de excepción diferente. En el ejemplo de arriba, *catch(ExceptionType exceptionReference)*, el tipo de excepción sería *ExceptionType*. Estos bloques catch se tienen que escribir en orden, de más específico a más general. Recordemos que la especificación o la generalización son conceptos de herencia: en la cadena de herencia, la superclase es más general y la subclase más específica. Por tanto, si quisiese capturar la excepción de tipo [*NullPointerException*](https://docs.oracle.com/javase/8/docs/api/java/lang/ArrayIndexOutOfBoundsException.html) y [*Exception*](https://docs.oracle.com/javase/8/docs/api/java/lang/Exception.html), como *Exception* es la superclase de todas las excepciones, *Exception* debería ir la última. *NullPointerException*, por tanto, debería ir delante.  
Si no mantenemos este orden y ponemos primero el bloque *catch* de tipo *Exception*, este bloque, debido al polimorfismo y la relación de herencia, sería capaz de capturar cualquier excepción, sea del tipo que sea. Al poner después el bloque *catch* de tipo *NullPointerException* nos daría el error de compilación `Unreachable catch block for NullPointerException. It is already handled by the catch block for Exception`: nunca va a ejecutarse el bloque *catch* de tipo *NullPointerException* porque ya se habrá capturado en el bloque *catch* de tipo *Exception*.

Si no hay relación de herencia entre los tipos de excepción de los bloques *catch*, no hace falta que vayan en ningún orden determinado. Por ejemplo, si queremos capturar *NullPointerException* y [*ArrayIndexOutOfBoundsException*](https://docs.oracle.com/javase/8/docs/api/java/lang/ArrayIndexOutOfBoundsException.html), no importa el orden de sus bloques *catch*.

### Ejemplo de diferentes bloques *catch* en el orden correcto
[Aquí](src/MainExcepcionesEleccionBloqueCatch.java) hay un ejemplo de un método que recorre un array de String imprimiendo cada una de las cadenas y su longitud. Una de las posiciones del array es null inicialmente, lo que provocará que se lance una excepción *NullPointerException*.  
Se inserta una cadena en esa posición null y se vuelve a recorrer el array, pero la condición del bucle for está mal y nos saldremos, por lo que se lanzará una excepción *ArrayIndexOutOfBoundsException*.  
Se recomienda que probéis a cambiar de orden los bloques *catch*:
* ¿Se puede poner el bloque *catch(Exception e)* el primero?
* ¿Se pueden intercambiar de orden los boques *catch(NullPointerException e)* y *catch(ArrayIndexOutOfBoundsException e)*?
* ¿Qué bloque *catch* captura la excepción que se lanza dentro del método `recorrerCadena()` al llamarla desde el *main*?

## Excepciones de tipo checked/unchecked (comprobadas/no-comprobadas)

En Java, existen dos tipos principales de excepciones: las excepciones **comprobadas (checked exceptions)** y las excepciones **no comprobadas (unchecked exceptions)**.

Las excepciones **comprobadas** son aquellas que **el compilador obliga a manejar explícitamente** en el código. Esto significa que, si un método puede generar una excepción comprobada, el código que lo llama debe manejar esa excepción o propagarla a un nivel superior. Ejemplos de excepciones comprobadas en Java son IOException y SQLException.

Por otro lado, las excepciones **no comprobadas** son aquellas que **el compilador no obliga a manejar** explícitamente. Esto significa que el código que llama a un método que genera una excepción no comprobada no tiene la obligación de manejarla. Ejemplos de excepciones no comprobadas en Java son NullPointerException e IllegalArgumentException. De no manejar una excepción no comprobada, el *runtime* recorrerá la pila de llamadas hasta el método *main* y al no encontrar su manejador, terminará la ejecución del programa y llamará al método *printStackTrace* de la excepción para mostrar todos los detalles del evento que ha provocado la terminación.

Las excepciones no comprobadas (unchecked) son todas aquellas que hereden de [*RuntimeException*](https://docs.oracle.com/javase/8/docs/api/java/lang/RuntimeException.html). En cambio, todas las excepciones que no hereden de *RuntimeException* pero hereden de [*Exception*](https://docs.oracle.com/javase/8/docs/api/java/lang/Exception.html) (todas las excepciones heredan de Exception), serán comprobadas (checked) y estaremos obligados a manejarlas.

## *throw* y *throws*

Además de ejecutar código que puede lanzar excepciones en los bloques *try* y capturarlas con los bloques *catch*, también vamos a poder [lanzar nuestras propias excepciones con la sentencia *throw*](https://docs.oracle.com/javase/tutorial/essential/exceptions/throwing.html). Si vamos a crear una nueva excepción, deberemos usar `throw new ExceptionType(parametros-constructor);`. En cambio, si lo que queremos es relanzar una excepción capturada en un bloque *catch*, podremos hacer algo así:

```java
try{
  //Bloque try que lanza excepción de tipo ExceptionType
}
catch(ExceptionType e){
  System.out.println("Exceción capturada. e="+e);
  throw e; //Así relanzamos la excepción que hemos capturado
}
```
Ya sea porque no capturamos una excepción, ya sea porque la capturamos y la relanzamos, [siempre que un método pueda lanzar una excepción, tiene que indicarlo](https://docs.oracle.com/javase/tutorial/essential/exceptions/declaring.html) con la cláusula `throws ExceptionType`.

Tenéis disponible un [ejemplo completo aquí](src/MainExcepcionesThrowYThrows.java) con un método que lanza una excepción con `throw` y que es llamado por 3 métodos distintos:
1. Uno que captura la excepción
2. Otro que no la captura y lo indica con la cláusula `throws`
3. Otro que captura la excepción y la relanza con `throw`, por lo que el método también debe llevar la cláusula `throws`.

## Crear nuestras propias excepciones
Además de todos los tipos de excepciones predefinidas en Java, nosotros podemos [crear nuestras propias excepciones](https://docs.oracle.com/javase/tutorial/essential/exceptions/creating.html) para notificar eventos en nuestros programas. Dado que una excepción es un evento excepcional que se representa por un objeto que es lanzado y capturado, para crear nuestras propias excepciones tendremos que crear la clase que servirá de plantilla para los objetos que representen nuestras excepciones. Para hacer esto, debemos crear una nueva clase, con la particularidad de que debe heredar de la clase [*Exception*](https://docs.oracle.com/javase/8/docs/api/java/lang/Exception.html) o de alguna de sus subclases. Para mejorar la legibilidad del código, es buena práctica añadir la palabra *Exception* al nombre de las clases que heredan de esta, tal y como hemos hecho en el siguiente ejempo:

Imaginemos que queremos hacer un programa que indique si un número es primo. Optaremos por la solución menos óptima posible: si *n* es el número proporcionado, intentaremos comprobar si *n* es divisible por algún número *x* entre *2* y *n-1*. En caso de que sea divisible, lanzaremos una excepción de tipo *NumeroDivisibleException* con el mensaje "*n* es divisible por *x*".

[Aquí](src/MainComprobarNumeroPrimo.java) tenéis el código del *main* y [aquí](src/NumeroDivisibleException.java) la clase *NumeroDivisibleException*.