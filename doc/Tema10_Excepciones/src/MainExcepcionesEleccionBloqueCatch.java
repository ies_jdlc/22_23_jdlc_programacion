public class MainExcepcionesEleccionBloqueCatch {
  static String cadena = "Hola";

  public static void main(String[] args) {
    // Vamos a forzar que se lancen distintos tipos de excepciones
    String[] saludos = { "Hola", null, "Adios" };

    System.out.println("# [main()] Llamo a recorrerArrayCadenas(saludos). La posición 1 de saludos es null...");
    recorrerArrayCadenas(saludos);

    // Rellenamos la posición 1 de saludos para que no se vuelva a lanzar la excepción NullPointerException 
    saludos[1] = "Qué tal?";
    System.out.println("# [main()] Llamo a recorrerArrayCadenas(saludos). Nos vamos a salir del array...");
    recorrerArrayCadenas(saludos);

    System.out.println("# [main()] Llamo a recorrerCadena(\"" + cadena + "\"). Nos vamos a salir de la cadena...");
    recorrerCadena(cadena);
  }

  public static void recorrerArrayCadenas(String[] arrayCadenas) {
    try {
      for (int i = 0; i <= arrayCadenas.length; i++) {
        System.out.println("# [recorrerArrayCadenas()] arrayCadenas[" + i + "]=" + arrayCadenas[i]);
        System.out.println("# [recorrerArrayCadenas()] arrayCadenas[" + i + "].length()=" + arrayCadenas[i].length());
      }
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("# [recorrerArrayCadenas()] Excepción posición de array incorrecta: " + e);
    } catch (NullPointerException e) {
      System.out.println("# [recorrerArrayCadenas()] Excepción puntero null: " + e);
    } catch (Exception e) {
      System.out.println("# [recorrerArrayCadenas()] Excepción genérica: " + e);
    } finally {
      System.out.println("# [recorrerArrayCadenas()] Bloque finally no hace nada");
    }
  }

  public static void recorrerCadena(String cadena) {
    try {
      for (int i = 0; i <= cadena.length(); i++) {
        System.out.println("# [recorrerCadena()] cadena[" + i + "]=" + cadena.charAt(i));
      }
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("# [recorrerCadena()] Excepción posición de array incorrecta: " + e);
    } catch (NullPointerException e) {
      System.out.println("# [recorrerCadena()] Excepción puntero null: " + e);
    } catch (Exception e) {
      System.out.println("# [recorrerCadena()] Excepción genérica: " + e);
    } finally {
      System.out.println("# [recorrerCadena()] Bloque finally no hace nada");
    }
  }
}
