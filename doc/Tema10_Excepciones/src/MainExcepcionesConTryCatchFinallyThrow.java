public class MainExcepcionesConTryCatchFinallyThrow {
  public static void main(String[] args) {
    try {
      System.out.println("[main()] Llamo a nullPointerMethod()");
      nullPointerMethod();
    } catch (NullPointerException e) {
      System.out.println("[main()] Excepción capturada NullPointerException en main");
      System.out.println("[main()] Re-lanzo la excepción NullPointerException en main");
      throw e;
    } finally {
      System.out.println("[main()] Ejecuto finally");
    }
  }
  public static void nullPointerMethod() {
    String palabra = null;
    try {
      System.out.println("[nullPointerMethod()] palabra.length()=" + palabra.length());
    } catch (NullPointerException e) {
      System.out.println("[nullPointerMethod()] catch NullPointer: " + e);
    } finally{
      System.out.println("[nullPointerMethod()] finally 1");
    }

    try {
      System.out.println("[nullPointerMethod()] palabra.length()=" + palabra.length());
    } catch (Exception e) {
      System.out.println("[nullPointerMethod()] catch Exception: " + e);
    } finally{
      System.out.println("[nullPointerMethod()] finally 2");
    }

    try {
      System.out.println("[nullPointerMethod()] palabra.length()=" + palabra.length());
    } catch (NullPointerException e) {
      System.out.println("[nullPointerMethod()] la capturo con catch NullPointer y la re-lanzo: " + e);
      throw e;
    }finally{
      System.out.println("[nullPointerMethod()] finally 3");
    }
  }
}
