public class MainExcepcionesThrowYThrows {
  static int contadorExcepcionesLanzadas = 0;

  public static void main(String[] args) {
    System.out.println("#[main()] Llamamos a metodoCapturaExcepcion()...");
    metodoCapturaExcepcion();

    System.out.println("#[main()] Llamamos a metodoNoCapturaExcepcion()...");
    try {
      metodoNoCapturaExcepcion();
    } catch (Exception e) {
      System.out.println("#[main()] Excepción de metodoNoCapturaExcepcion() capturada. e=" + e);
    }

    System.out.println("#[main()] Llamamos a metodoCapturaYRelanzaExcepcion()...");
    try {
      metodoCapturaYRelanzaExcepcion();
    } catch (Exception e) {
      System.out.println("#[main()] Excepción de metodoCapturaYRelanzaExcepcion() capturada. e=" + e);
    }
  }

  public static void metodoLanzaExcepcion() throws Exception {
    System.out.println("###[metodoLanzaExcepcion()] throw new Exception(...)");
    throw new Exception("[" + (++contadorExcepcionesLanzadas) + "] Excepción lanzada desde metodoLanzaExcepcion()");
  }

  public static void metodoCapturaExcepcion() {
    try {
      metodoLanzaExcepcion();
    } catch (Exception e) {
      System.out.println("##[metodoCapturaExcepcion()] Excepción caputrada. e=" + e);
    }
  }

  public static void metodoNoCapturaExcepcion() throws Exception {
    metodoLanzaExcepcion();
  }

  public static void metodoCapturaYRelanzaExcepcion() throws Exception {
    try {
      metodoLanzaExcepcion();
    } catch (Exception e) {
      System.out.println("##[metodoCapturaYRelanzaExcepcion()] Excepción caputrada. e=" + e);
      System.out.println("##[metodoCapturaYRelanzaExcepcion()] throw e");
      throw e;
    }
  }
}
