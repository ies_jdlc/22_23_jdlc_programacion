public class MainComprobarNumeroPrimo {
  public static void main(String[] args) {
    for (int numero = 1; numero < 20; numero++) {
      try{
        if(comprobarNumeroPrimo(numero)){
          System.out.println("El numero " + numero + " es primo");
        }
      }catch(NumeroDivisibleException e){
        System.out.println(e.getMessage());
      }
    }
  }

  public static boolean comprobarNumeroPrimo(int numero) throws NumeroDivisibleException{
    for(int i=2; i<numero; i++){
      if(numero%i==0){
        throw new NumeroDivisibleException("El numero " + numero + " es divisble entre "+ i);
      }
    }
    return true;    
  }

}
