public class NumeroDivisibleException extends Exception{
  public NumeroDivisibleException(String mensaje){
    super(mensaje);
  }
}
