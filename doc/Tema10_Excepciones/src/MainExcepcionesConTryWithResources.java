import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MainExcepcionesConTryWithResources {
  static final String NOMBRE_FICHERO = "MainExcepcionesConTryWithResources.java";
  static final String NOMBRE_FICHERO_INVENTADO = "INVENT.TXT";

  public static void main(String[] args) {
    System.out.println("# [main()] Voy a contar las líneas del fichero "
        + NOMBRE_FICHERO + " con un try-catch-finally normal...");
    System.out.println("# [main()] contarLineasFichero(\"" + NOMBRE_FICHERO + "\") ha devuelto: "
        + contarLineasFichero(NOMBRE_FICHERO) + "\n");

    System.out.println("# [main()] Voy a contar las líneas del fichero "
        + NOMBRE_FICHERO + " con un try-with-resources...");
    System.out.println("# [main()] contarLineasFicheroTryWithResources(\"" + NOMBRE_FICHERO + "\") ha devuelto: "
        + contarLineasFicheroTryWithResources(NOMBRE_FICHERO) + "\n");

    System.out.println("# [main()] Voy a contar las líneas del fichero "
        + NOMBRE_FICHERO_INVENTADO + " con un try-catch-finally normal...");
    System.out.println("# [main()] contarLineasFichero(\"" + NOMBRE_FICHERO_INVENTADO + "\") ha devuelto: "
        + contarLineasFichero(NOMBRE_FICHERO_INVENTADO) + "\n");

    System.out.println("# [main()] Voy a contar las líneas del fichero "
        + NOMBRE_FICHERO_INVENTADO + " con un try-with-resources...");
    System.out.println("# [main()] contarLineasFicheroTryWithResources(\"" + NOMBRE_FICHERO_INVENTADO + "\") ha devuelto: "
        + contarLineasFicheroTryWithResources(NOMBRE_FICHERO_INVENTADO) + "\n");
  }

  public static int contarLineasFichero(String ruta) {
    FileReader lector = null;
    BufferedReader lectorConBuffer = null;
    try {
      System.out.println("## [contarLineasFichero()] Creo FileReader:");
      lector = new FileReader(ruta);
      System.out.println("## [contarLineasFichero()] Creo BufferedReader:");
      lectorConBuffer = new BufferedReader(lector);

      System.out.println("## [contarLineasFichero()] Cuento líneas...");
      int contadorLineas = 0;
      while (lectorConBuffer.readLine() != null) {
        contadorLineas++;
      }
      System.out.println("## [contarLineasFichero()] El fichero " + NOMBRE_FICHERO
          + " tiene " + contadorLineas + " líneas.");
      return contadorLineas;

    } catch (NullPointerException e) {
      System.out.println("## [contarLineasFichero()] Excepción de NullPointerException: " + e);
    } catch (FileNotFoundException e) {
      System.out.println("## [contarLineasFichero()] Excepción de fichero no encontrado: " + e);
    } catch (IOException e) {
      System.out.println("## [contarLineasFichero()] Excepción de Entrada/Salida: " + e);
    } finally {
      System.out.println("## [contarLineasFichero()] Ejecuto finally");
      // Cerrando el BufferedReader ya se cierra el stream que se le haya pasado en el
      // constructor:
      // https://stackoverflow.com/a/1388627/1502016
      // Llamar a lectorConBuffer.close() es suficiente, no hace falta llamar también
      // a lector.close().

      // Si comentamos el siguiente bloque de código en el que, si lectorConBuffer no
      // es null,
      // llamamos a lectorConBuffer.close(), el compilador nos daría un warning
      // indicando que
      // hay una pérdida de recursos al no cerrarse el fichero:
      // "Resource leak: 'lectorConBuffer' is not closed at this location"
      if (lectorConBuffer != null) {
        try {
          lectorConBuffer.close();
        } catch (IOException e) {
          System.out.println("## [contarLineasFichero()] Excepción de Entrada/Salida "
              + "al llamar a lectorConBuffer.close(): " + e);
        }
      }
    }
    return -1;
  }

  public static int contarLineasFicheroTryWithResources(String ruta) {

    try (FileReader lector = new FileReader(ruta);
        BufferedReader lectorConBuffer = new BufferedReader(lector);) {
      System.out.println("## [contarLineasFicheroTryWithResources()] Cuento líneas...");
      int contadorLineas = 0;
      while (lectorConBuffer.readLine() != null) {
        contadorLineas++;
      }
      System.out.println("## [contarLineasFicheroTryWithResources()] El fichero " + NOMBRE_FICHERO + " tiene "
          + contadorLineas + " líneas.");
      return contadorLineas;

    } catch (NullPointerException e) {
      System.out.println("## [contarLineasFicheroTryWithResources()] Excepción de NullPointerException: " + e);
    } catch (FileNotFoundException e) {
      System.out.println("## [contarLineasFicheroTryWithResources()] Excepción de fichero no encontrado: " + e);
    } catch (IOException e) {
      System.out.println("## [contarLineasFicheroTryWithResources()] Excepción de Entrada/Salida: " + e);
    } finally {
      System.out.println("## [contarLineasFicheroTryWithResources()] Ejecuto finally");
    }
    return -1;
  }

}
