Ficheros
===
Table of concepts:
- [Ficheros](#ficheros)
  - [Introducción](#introducción)
  - [Acceso a ficheros con Java](#acceso-a-ficheros-con-java)
    - [Tipos de flujos/streams](#tipos-de-flujosstreams)
    - [Clases para lectura/escritura de ficheros en Java](#clases-para-lecturaescritura-de-ficheros-en-java)
    - [Lectura y escritura con buffer](#lectura-y-escritura-con-buffer)
    - [Paquete avanzado *java.nio*](#paquete-avanzado-javanio)


## Introducción
Un fichero o archivo es un conjunto de bytes almacenados en un dispositivo ([Archivo Informático-Wikipedia](https://es.wikipedia.org/wiki/Archivo_(inform%C3%A1tica)))

Operaciones con ficheros:
* Creación
* Lectura
* Escritura
* Eliminación

Tipos de acceso a ficheros:
* Secuencial   
  Si el acceso es secuencial, significa que no podremos leer la dirección que queramos directamente, sino que tendremos que recorrer todo el fichero hasta llegar al dato que queramos.  
  Los ejemplos más claros serían los antiguos [casetes de música](https://es.wikipedia.org/wiki/Casete) (la necesidad de rebobinarlos con un reproductor o con un boli bic), las cintas [VHS](https://es.wikipedia.org/wiki/VHS) y la [cinta magnética de almacenamiento de datos](https://es.wikipedia.org/wiki/Cinta_magn%C3%A9tica_de_almacenamiento_de_datos). Estas cintas magnéticas de almacenamiento de datos siguen utilizándose ampliamente a día de hoy para almacenar grandes cantidades de datos que se leen muy rara vez, por ejemplo para copias de seguridad ([estudio de sistemas de almacenamiento en computación de altas prestaciones (inglés)](https://doi.org/10.14529/jsfi180103)).
* Aleatorio  
  Si el acceso es aleatorio (random access en inglés), podemos acceder directamente a la posición del fichero deseada, sin necesidad de pasar por todas las posiciones anteriores. Hoy en día estamos acostumbrados a que todos los dispositivos que utilizamos permiten acceso aleatorio. Pero hubo una época en la que esto no era tan frecuente y, de hecho, se llamó memoria de acceso aleatorio a la memoria principal de un computador, conocida como [RAM (Random Access Memory)](https://es.wikipedia.org/wiki/Memoria_de_acceso_aleatorio)

## Acceso a ficheros con Java

Java nos permite acceder a ficheros como si fuesen un flujo/stream de datos, permitiéndonos ignorar el tipo de dispositivo donde estén almacenados.  

Flujo de lectura (Input stream):

![Flujo de lectura desde un fichero](https://docs.oracle.com/javase/tutorial/figures/essential/io-ins.gif "Flujo de lectura")

Flujo de escritura (Output stream):

![Flujo de escritura a un fichero](https://docs.oracle.com/javase/tutorial/figures/essential/io-outs.gif "Flujo de escritura")

Podemos ver estos [flujos como si se tratase de una tubería por la que fluyen los datos](https://docs.oracle.com/javase/tutorial/essential/io/streams.html). Si queremos escribir en un fichero tendríamos que introducir datos en la *tubería*. Si vamos a leer, tendremos que sacar datos de la *tubería*, como si abriésemos un grifo.  
Esta abstracción nos va a permitir utilizar un enfoque muy parecido cuando estemos leyendo un fichero, recibiendo datos por la red o cualquier otra forma de recibir datos. También vamos a trabajar de forma similar cuando escribamos un fichero o enviemos datos por la red.

### Tipos de flujos/streams

Hay varios tipos de flujos dependiendo del tipo de dato que vayamos a leer o escribir. Los dos principales son los flujos de caracteres (archivos de texto) y los flujos de bytes (archivos binarios).  
Pero también existen flujos para leer/escribir datos de los [tipos primitivos de Java (float, int, boolean, etcétera) y Strings](https://docs.oracle.com/javase/tutorial/essential/io/datastreams.html), y flujos para leer y escribir [objetos que implementen la interfaz *Serializable*](https://docs.oracle.com/javase/tutorial/essential/io/objectstreams.html).  

### Clases para lectura/escritura de ficheros en Java

Las clases para entrada y salida (input/output en inglés) se encuentran en el *package* **java.io** ([tutorial oficial](https://docs.oracle.com/javase/8/docs/api/java/io/package-summary.html), [java.io-Wikipedia (inglés)](https://en.wikipedia.org/wiki/Java_Platform,_Standard_Edition#java.io)).  
* [*FileReader*](https://docs.oracle.com/javase/8/docs/api/java/io/FileReader.html) y [*FileWriter*](https://docs.oracle.com/javase/8/docs/api/java/io/FileReader.html) son las clases para **caracteres** y heredan de las clases abstractas *Reader* y *Writer* ([tutorial oficial](https://docs.oracle.com/javase/tutorial/essential/io/charstreams.html))

* [*FileInputStream*](https://docs.oracle.com/javase/8/docs/api/java/io/FileInputStream.html) y [*FileOutputStream*](https://docs.oracle.com/javase/8/docs/api/java/io/FileOutputStream.html) son las clases para trabajar con **bytes** y heredan de las clases abstractas *InputStream* y *OutputStream* ([tutorial oficial](https://docs.oracle.com/javase/tutorial/essential/io/bytestreams.html)). 

* [DataInputStream](https://docs.oracle.com/javase/8/docs/api/java/io/DataInputStream.html) y [DataOutputStream](https://docs.oracle.com/javase/8/docs/api/java/io/DataOutputStream.html) son las clases para trabajar con **tipos primitivos y Strings** e implementan las interfaces [*DataInput*](https://docs.oracle.com/javase/8/docs/api/java/io/DataInput.html) y [*DataOutput*](https://docs.oracle.com/javase/8/docs/api/java/io/DataOutput.html).

* [ObjectInputStream](https://docs.oracle.com/javase/8/docs/api/java/io/ObjectInputStream.html) y [ObjectOutputStream](https://docs.oracle.com/javase/8/docs/api/java/io/ObjectOutputStream.html) son las clases para trabajar con **objetos** y heredan de las clases abstractas [InputStream](https://docs.oracle.com/javase/8/docs/api/java/io/InputStream.html) y [OutputStream](https://docs.oracle.com/javase/8/docs/api/java/io/OutputStream.html) 



Las clases anteriores nos permiten leer/escribir byte a byte de un fichero binario, o caracter a caracter de un fichero de texto. También podemos leer en un array o escribir desde un array un tamaño fijo de bytes/caracteres. Este tipo de lectura y escritura no es muy eficiente, ya que el acceso a los ficheros es una operación muy lenta comparada con la velocidad del procesador y la memoria.  

### Lectura y escritura con buffer
Para mejorar el rendimiento de la lectura, se utilizan [flujos con buffer](https://docs.oracle.com/javase/tutorial/essential/io/buffers.html), de manera que cuando vamos a leer de un fichero, un flujo con buffer no lee un único byte/caracter, sino que lee un bloque, lo guarda en un buffer o zona de memoria reservada, y cada vez que queramos leer un byte/caracter, lo leeremos de este buffer. Este procedimiento es mucho más rápido y eficiente que acceder al dispositivo de almacenamiento. Cuando este buffer se haya leído por completo, se volverá a acceder al dispositivo para leer el siguiente bloque de información. De forma análoga, la escritura también puede utilizar un buffer para mejorar su rendimiento.  
Contamos con 4 clases para leer y escribir ficheros binarios y de texto utilizando buffers:
* [BufferedInputStream](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedInputStream.html) y [BufferedOutputStream](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedOutputStream.html) para acceso de byte
* [BufferedReader](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedReader.html) y [BufferedWriter](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedWriter.html) para acceso de caracteres.

Gracias a este uso de buffer, vamos a poder [leer líneas completas](https://docs.oracle.com/javase/8/docs/api/java/io/BufferedReader.html#readLine--) desde un fichero de texto, en vez de tener que leer caracter a caracter o un bloque de tamaño predeterminado que puede ser más pequeño o más grande que la siguiente línea.

### Paquete avanzado *java.nio*
 Algunas clases de entrada y salida más avanzadas se encuentran en el *package* **java.nio** ([tutorial oficial](https://docs.oracle.com/javase/8/docs/api/java/nio/package-summary.html), [java.nio-Wikipedia](https://en.wikipedia.org/wiki/Non-blocking_I/O_(Java))), pero no lo cubriremos en este curso.