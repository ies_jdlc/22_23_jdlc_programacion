import java.util.ArrayList;

public class MainPersona {
  public static void main(String[] args) {
    System.out.println("### MainPersona.main()");

    Persona anonimo = new Persona();
    System.out.println("anonimo=" + anonimo);
    Persona fer = new Persona("Fernando", 38);
    System.out.println("fer=" + fer);
    Persona maria = new Persona("Maria", 30);
    System.out.println("maria=" + maria);

    ArrayList<Persona> listaPersonas = new ArrayList<Persona>();
    listaPersonas.add(anonimo);
    listaPersonas.add(fer);
    listaPersonas.add(maria);

    System.out.println("==> Listado Personas(" + (Object) listaPersonas + "): ");
    imprimirListaPersonas(listaPersonas);

    System.out.println("==> Icrementamos la edad de las personas(" + (Object) listaPersonas + ").");
    incrementarEdad(listaPersonas);

    System.out.println("==> Listado Personas(" + (Object) listaPersonas + "): ");
    imprimirListaPersonas(listaPersonas);

    System.out.println("Bye, bye...");
  }

  public static void imprimirListaPersonas(ArrayList<Persona> listaPersonas) {
    Persona referenciaPersona = null;

    for (int i = 0; i < listaPersonas.size(); i++) {
      referenciaPersona = listaPersonas.get(i);
      imprimirPersona(i, referenciaPersona);
    }
  }

  public static void imprimirPersona(int posicion, Persona persona) {
    System.out.println("Persona " + posicion + "[" + persona.toString() + "]) " + persona.nombre + ", "
        + persona.edad + " años");
  }

  private static void incrementarEdad(ArrayList<Persona> listaPersonas) {
    for (Persona referenciaPersona : listaPersonas) {
      if (referenciaPersona.edad == null) {
        referenciaPersona.edad = 18;
      } else {
        referenciaPersona.edad++;
      }
    }
  }
}
