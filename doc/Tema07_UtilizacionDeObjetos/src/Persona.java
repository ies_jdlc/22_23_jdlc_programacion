public class Persona {
  String nombre;
  Integer edad;

  public Persona() {}

  public Persona(String nombre, Integer edad) {
    this.nombre = nombre;
    this.edad = edad;
  }
}
