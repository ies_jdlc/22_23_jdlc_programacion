public class EjemploMetodos {
  public static void main(String[] args) {
    ClaseEjemplo ejemplo = new ClaseEjemplo();
    ejemplo.setX(5);
    ejemplo.setY(8);
    System.out.println("ejemplo.x = " + ejemplo.getX() + ", ejemplo.y = " + ejemplo.getY());
    System.out.println("ejemplo.multiplicaXconY() = " + ejemplo.multiplicaXconY());

    int resultado = ClaseEjemplo.multiplicaParametros(4, 5);
    System.out.println("ClaseEjemplo.multiplicaParametros(4, 5) = " + resultado);
  }

}
