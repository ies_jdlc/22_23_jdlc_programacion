Utilización de Objetos
===

Table of concepts:
- [Utilización de Objetos](#utilización-de-objetos)
  - [Arrays](#arrays)
  - [Ámbito/Tipo de variables](#ámbitotipo-de-variables)
  - [Clases Envoltorio, wrapper classes](#clases-envoltorio-wrapper-classes)
- [Métodos](#métodos)
  - [Introducción a los métodos](#introducción-a-los-métodos)
  - [Modificadores](#modificadores)
    - [Modificadores de acceso](#modificadores-de-acceso)
    - [Otros modificadores](#otros-modificadores)
  - [Tipo de retorno](#tipo-de-retorno)
  - [Nombre del método](#nombre-del-método)
  - [Lista de parámetros/argumentos](#lista-de-parámetrosargumentos)
- [Sobrecarga de métodos](#sobrecarga-de-métodos)
  - [Firma de un método](#firma-de-un-método)
  - [Constructores](#constructores)
  - [Palabra reservada/keyword *this*](#palabra-reservadakeyword-this)
  - [Paso de parámetros: por valor o por referencia](#paso-de-parámetros-por-valor-o-por-referencia)
  - [Memoria en Java: stack y heap, referencia vs objeto, garbage collector (recolector de basura)](#memoria-en-java-stack-y-heap-referencia-vs-objeto-garbage-collector-recolector-de-basura)
    - [Cuadro resumen *stack* vs *heap* (basado en apuntes de Catha)](#cuadro-resumen-stack-vs-heap-basado-en-apuntes-de-catha)
  - [*package* en Java](#package-en-java)


## Arrays
Los arrays son contenedores de objetos. El número de objetos que puede contener un array es fijo y no se puede modificar una vez que se ha creado.
```java
// Creación de un array de int's:
int[] arrayDeTresNumerosA = new int[3];

// Forma NO recomendada:
int arrayDeTresNumerosB[] = new int[3];

// Abreviación para crear e inicializar un array:
int[] arrayDeTresNumerosC = {1, 2, 3};
```
[Documentación oficial Arrays](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html)

## Ámbito/Tipo de variables
Podemos considerar 3 tipos de variables
* Variables locales/automáticas, argumentos métodos  
Se declaran dentro del cuerpo de un método o se reciben como parámetros, son visibles sólo dentro del método y se almacenan en el *stack*.
* Variables de instancia  
  Se declaran como atributos de una clase, fuera de ningún método. Cada vez que se crea un objeto, se crean sus variables de instancia. Se almacenan en el *heap*.
  
* Variables de clase (static)  
  Se declaran usando el modificador *static* y dentro de una clase pero fuera de sus métodos. Estas variables son únicas por cada clase y se comparten por todos los objetos de esa clase.

[Documentación oficial Variables](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html)

## Clases Envoltorio, wrapper classes
En Java, cada tipo primitivo tiene su correspondiente clase, que encapsula el tipo primitivo en una clase. Teniendo en cuenta que en Java las colecciones y otras herramientas del lenguaje sólo trabajan con objetos, vamos a necesitar estas clases envoltorio para representar tipos primitivos.  

|**Primitive Data Type** | **Wrapper Class**|
|------------------------|------------------|
|byte |	Byte |
|short| 	Short|
|int| 	Integer|
|long| 	Long|
|float| 	Float|
|double| 	Double|
|boolean| 	Boolean|
|char| 	Character|

[Java Wrapper Classes](https://www.w3schools.com/java/java_wrapper_classes.asp)


Métodos
===

Un método es una subrutina que manipula los datos de la clase y/o los proporcionados como parámetros. En la práctica, un método es un bloque de código que se ejecuta cuando se invoca el método.  

## Introducción a los métodos
Los métodos pueden recibir valores/variables de entrada y devolver un valor de salida.  
La forma de declarar un método, dentro de un fichero .java, sigue la siguiente sintaxis:
```
<modificador-de-acceso> <otros-modificadores> <tipo-de-retorno> <nombre-método> (<lista-argumentos>) {

  <bloque de código>

 }
```
## Modificadores
En Java se definen 2 tipos de [modificadores](https://www.w3schools.com/java/java_modifiers.asp): *access modifiers* (modificadores de acceso) y *non-access modifiers* (modificiadores que no son de acceso).

### Modificadores de acceso  
  Estos modificadores especifican quién puede acceder a una clase, un método o un atributo. Por ahora nos centraremos en las implicaciones de estos modificadores sobre los métodos.  
  * *public*: Un método public puede ser invocado desde cualquier clase.
  * *protected*: Pueden ser invocados desde cualquier clase del mismo package donde se define y desde cualquier subclase que herede de esta.
  * *private*: Sólo se puede invocar desde la clase donde se define el método.
  * Default/Package: Sólo se puede invocar desde las clases que pertenezcan al mismo package. Para conseguir que un método tenga este tipo de acceso **no se debe especificar ningún modificador de acceso**, es el tipo de acceso por defecto.

### Otros modificadores  
  Estos modificadores especifican otras características de clase, método o atributo.
  * *final*: Indica que ha alcanzado su estado definitivo y por tanto no se puede modificar. Si una clase es final, no se puede heredar de ella. Si un método es final, no se puede sobreescribir. Si una variable es final, una vez inicializado su valor, no se puede modificar.
  * *abstract*: Si una clase es abstract, no se podrá crear una instancia de esa clase, sólo de sus clases derivadas mediante herencia. Si un método es abstract, no definirá su comportamiento (bloque de código). Una clase con un método abstracto debe ser abstracta.
  * *static*: un atributo o método static pertenece a la clase y no a cada uno de los objetos, por lo que no hace falta una instancia para acceder a un atributo static ni para llamar a un método static.
  
## Tipo de retorno
Los métodos pueden devolver un valor al terminar su ejecución. El *tipo* del valor que van a devolver se especifica antes del nombre del método. Si un método no va a devolver nada, se especifica el tipo especial *void*.  
Desde dentro del método, para devolver un valor se utiliza *return <valor/variable>*.

## Nombre del método
Como hemos visto en el tema anterior, los métodos, al igual que las variables, se nombran utilizando el formato *camelCase*. Es **muy importante** utilizar nombres que indiquen lo que hace el método, incluyendo normalmente un verbo y un sustantivo.

## Lista de parámetros/argumentos
Los métodos pueden recibir una lista de parámetros que utilizarán para realizar las operaciones correspondientes. La lista de parámetros puede ser vacía cuando un método no necesita recibir nada. En ese caso, no habrá nada entre los paréntesis. Si recibe parámetros, estos irán en pares que indicarán el tipo y el nombre de cada parámetro, separando estos pares con comas.

# Sobrecarga de métodos
Se dice que un método está sobrecargado cuando una clase tiene dos o más métodos con el mismo nombre. Esto puede ser útil cuando queremos ejecutar una operación con un objeto, pero el código exacto es diferente en función del número y/o tipo de argumentos que se le proporcionen.  
Para que la sobrecarga de un método sea válida, las firmas deben ser diferentes.  
## Firma de un método
La *firma de un método* está compuesta por el nombre y los tipos de los parámetros. El tipo de retorno y los nombres de los parámetros no forman parte de la firma.

Ejemplo:
La clase *Math* define varios métodos *max* sobrecargados:
```java
// Métodos sobrecargados:
public static double max(double a, double b); // 1
public static float max(float a, float b);    // 2
public static long max(long a, long b);       // 3
public static int max(int a, int b);          // 4
```
Las firmas de esos métodos se pueden obtener quedándonos con el nombre y los tipos de sus parámetros:
```
1: max(double, double)
2: max(float, float)
3: max(long, long)
4: max(int, int)
```
Ejemplos de sobrecargas inválidas:
```java
// Métodos inválidos por tener la misma firma:
public static float max(double a, double b);      // 5 -> ERROR: Misma firma que 1: max(double, double);
public static double max(double uno, double dos); // 6 -> ERROR: Misma firma que 1: max(double, double);
```
[Defining Methods: naming, overloading and signature](https://docs.oracle.com/javase/tutorial/java/javaOO/methods.html)

## Constructores
Los atributos de los objetos se inicializan a valores por defecto en función de su tipo: boolean a *false*, referencias a objetos a *null*, tipos numéricos, char y byte a *0*.  
Las variables locales deben inicializarse antes de acceder a su valor. Si no se hace, el programa dará un error en tiempo de compilación.  

Más allá de los valores por defecto, podemos querer inicializar los atributos de un objeto en el momento de crearlo. Para este propósito concreto existe un método especial que se llama *constructor*.

```java
public class Persona {
  String nombre;
  Integer edad;

  public Persona() {}

  public Persona(String nombre, Integer edad) {
    this.nombre = nombre;
    this.edad = edad;
  }
}
```
```java
public class MainPersona {
  public static void main(String[] args) {
    Persona anonimo = new Persona();
    Persona fer = new Persona("Fernando", 38);
  } 
}
```
En el ejemplo anterior, la clase *Persona* tiene 2 constructores: uno vacío que no recibe argumentos y otro que recibe *nombre* y *edad* como argumentos.  
En el *main*, al crear el objeto de tipo Persona referenciado por *anonimo*, utilizamos el constructor vacío (no pasamos ningún argumento al constructor invocado con *new*). Por tanto, *anonimo.nombre* y *anonimo.edad* serán *null*.  
En cambio, al crear el objeto referenciado por *fer*, utilizamos el segundo constructor que recibe un *String* ("Fernando") y un *Intenger* (38). Por tanto, *fer.nombre* será "Fernando" y *fer.edad* será 38.

Resumen:
* Para invocar el constructor se utiliza la palabra reservada *new*
* El constructor se ejecuta exclusivamente en el momento de crear un objeto
* Sirve para inicializar los valores de los atributos
* El constructor es un método que no tiene tipo de retorno y su nombre coincide con el nombre de la clase
* Debe ser public para que se pueda utilizar desde fuera de la clase
* Puede recibir argumentos o no (constructor vacío)
* Una clase puede definir varios constructores siguiendo las reglas de la sobrecarga de métodos
* Si una clase no define ningún constructor, Java genera el constructor por defecto. Este constructor es vacío (no recibe argumentos) e inicializa los atributos del objeto a los valores por defecto mencionados anteriormente (false, null y 0). Si nosotros definimos un constructor para una clase, Java ya no genera el constructor por defecto para esa clase.

## Palabra reservada/keyword *this*

*this* es una palabra reservada en Java que hace referencia al objeto sobre el que se está ejecutando el código. Sólo debe utilizarse cuando haya dos variables con el mismo nombre, siendo una de ellas una variable de instancia. Para hacer referencia a esta, podremos utilizar *this.\<nombre-variable\>*. Hemos visto este uso en el segundo constructor de la clase *Persona* en el ejemplo anterior. Para discernir qué variable es la variable de instancia nombre y cuál es la variable recibida como argumento por el constructor, hemos utilizado lo siguiente:
> this.nombre = nombre;

Así hemos podido indicar que queremos asignar a la variable de instancia *nombre*, que pertenece al objeto para el que se está ejecutando el constructor, el valor de la variable *nombre* recibida como parámetro.

Otro caso en el que se puede utilizar *this* es para llamar a otro constructor desde un constructor. Para ver este otro uso, imaginemos que en la clase *Persona*, además de *nombre* y *edad*, tenemos también el atributo *rol*. Si quisiésemos crear un constructor que también inicialice esta otra variable de instancia, **podríamos duplicar** el cuerpo del 2º constructor que inicializa *nombre* y *edad*, y además añadir otra línea que inicializase rol:
> this.rol = rol;

Como una de las reglas principales en la programación orientada a objetos es evitar la duplicidad de código, en vez de duplicar esas 2 líneas del otro constructor, podemos invocar a ese constructor desde el nuevo constructor:

```java
public class Persona {
  String nombre;
  Integer edad;
  String rol;

  public Persona() {}

  public Persona(String nombre, Integer edad) {
    this.nombre = nombre;
    this.edad = edad;
  }

  public Persona(String nombre, Integer edad, String rol) {
    this(nombre, edad);
    this.rol = rol;
  }
}
```

## Paso de parámetros: por valor o por referencia

*Resumen basado en el documento [ud7_Funciones.pdf](https://github.com/lionel-ict/ApuntesProgramacion/blob/master/Unidad%207%20Funciones/ud7_Funciones.pdf) del repositorio [ApuntesProgramacion](https://github.com/lionel-ict/ApuntesProgramacion) de [lionel-ict](https://github.com/lionel-ict)*

Existen dos tipos de parámetros y es importante comprender la diferencia.

* Parámetros de tipo primitivo (paso por valor): Como int, double, boolean, char, etc. En este caso se pasan por valor. Es decir, el valor se copia al parámetro y por lo tanto si se modifica dentro de la función esto no afectará al valor fuera de ella porque son variables distintas.

* Parámetros de tipo clase/objeto (paso por referencias): Como los objetos de tipo String, los Arrays, etc. En este caso no se copia el objeto sino que se le pasa a la función una referencia al objeto original (un puntero). Por ello desde la función se accede directamente al objeto que se encuentra fuera. Los cambios que hagamos dentro de la función afectarán al objeto.

## Memoria en Java: stack y heap, referencia vs objeto, garbage collector (recolector de basura)
Prácticamente todas las aplicaciones, escritas en cualquier lenguaje de programación, tienen 2 zonas principales de memoria: el *stack* y el *heap*. Estos conceptos están íntimamente relacionados con la gestión de los procesos por el sistema operativo, así que los veréis en otros muchos entornos.

* **Stack**: Es una zona de memoria exclusiva para cada thread/hilo de ejecución. Funciona como un stack, de ahí su nombre.
* **Heap**: Es una zona de memoria sin estructura específica, de ahí su nombre heap/montón. Es una zona compartida por todos los threads de un proceso. Las aplicaciones van reservando bloques de memoria de esta zona, ya sea explícitamente en lenguajes con gestión de memoria como C/C++, o implícitamente, en lenguajes como Java.

En el caso concreto de Java, todas las variables de tipo primitivo se almacenan en el *stack*. Las variables cuyo tipo es un clase, también llamadas referencias, tambiés se almacenan en el stack, pero los objetos a los que pueden apuntar estas referencias (cuando no son *null*) siempre se almacenan en el *heap*.

Es decir, en Java solamente aquellos objetos creados con *new* se almacenan en el heap, aunque las referencias con los que los manejamos se encuentran en el *stack*.  
Estas referencias son equivalentes a los punteros que existen en otros lenguajes de programación, con la diferencia de que en Java tenemos muy restringidas las operaciones que podemos hacer con estas referencias. La ventaja es que es muy difícil cometer error en la gestión de memoria en Java. Estos errores son muy frecuentes en lenguajes como C/C++ ya que la gestión de memoria dinámica requiere conocimientos avanzados en cuanto la aplicación se amplía y complejiza.

Además de las variables de tipo primitivo y todas las referencias a objetos, en el *stack* también se almacenan la dirección de retorno de los métodos (por dónde debe continuar la ejecución una vez termine de ejecutarse el método) y los parámetros que se le pasen al método al llamarlo.

En la siguiente captura de pantalla, en la que se muestra el debugger durante la ejecución de [*MainPersona.java*](src/MainPersona.java), se puede ver cómo en la sección **CALL STACK** del panel izquierdo se han acumulado las llamadas a `main(String[])`, desde ahí a `imprimirListaPersonas(ArrayList)` y de ahí a `imprimirPersona(int,Persona)`. Como la captura de pantalla se ha tomado dentro del método *imprimirPersona*, en la sección **VARIABLES** sólo aparecen *posicion* y *persona*, que son los parámetros del método.  
Si continuamos con la ejecución, veremos cómo al llamar a *incrementarEdad(listaPersonas)*, el atributo *edad* de los objetos se modifican. He añadido la llamada al método *toString()* heredado de Object que imprime el nombre de la clase, una arroba y el hashCode en formato hexadecimal. Así podemos ver que se trata del mismo objeto, aunque modifiquemos su edad.


![Captura pantalla VSCode debugger](img/2023-05-02_VSCode_CallStack_MainPersona.png)

Ejecución de *MainPersona.java*:

```
### MainPersona.main()
anonimo=Persona@7344699f
fer=Persona@6b95977
maria=Persona@7e9e5f8a
==> Listado Personas([Persona@7344699f, Persona@6b95977, Persona@7e9e5f8a]): 
Persona 0[Persona@7344699f]) null, null años
Persona 1[Persona@6b95977]) Fernando, 38 años
Persona 2[Persona@7e9e5f8a]) Maria, 30 años
==> Icrementamos la edad de las personas([Persona@7344699f, Persona@6b95977, Persona@7e9e5f8a]).
==> Listado Personas([Persona@7344699f, Persona@6b95977, Persona@7e9e5f8a]): 
Persona 0[Persona@7344699f]) null, 18 años
Persona 1[Persona@6b95977]) Fernando, 39 años
Persona 2[Persona@7e9e5f8a]) Maria, 31 años
Bye, bye...
```
### Cuadro resumen *stack* vs *heap* (basado en apuntes de [Catha](https://github.com/creyDaweb))

||Stack|Heap|
|-|-|-|
|Alamacena|Variables locales, de tipo primitivo y referencias a objetos | Objetos (creados con *new*)|
|Tamaño| Virtualmente infinito, tamaño máximo configurable. Cuando se agota el espacio: excepción *StackOverflowError*| Virtualmente infinito, tamaño máximo configurable. Cuando se agota el espacio: excepción *OutOfMemoryError*.|
|Liberación de memoria| No hace falta liberar: cuando un método termina de ejecutar, deja la memoria que había utilizado disponible para los siguientes métodos que vayan a ejecutarse. | No hace falta liberar: cuando no quedan referencias "apuntando" a un objeto en el heap, anteriormente creado con *new*, el **garbage collector** se encarga de liberar la memoria que ocupaba ese objeto.|
|Estructura| LIFO, es una pila: las nuevas variables y las llamadas a métodos utilizan las siguientes posiciones, creciendo de abajo hacia arriba. Cuando se libera una variable local/automática, saliendo del bloque en el que existe, esa posición queda libre. Lo mismo ocurre cuando se termina la ejecución de un método.| El heap no tiene una estructura concreta: proporciona bloques de memoria sin especificar más detalles. El Java runtime y el sistema operativo se encargarán de hacerlo lo más eficientemente posible.|

Se recomienda encarecidamente revisar el ejemplo con figura y tabla explicativa disponible en la página [Stack Memory and Heap Space in Java](https://www.baeldung.com/java-stack-heap).

## *package* en Java

*Resumen basado en una conversación con Chat-GPT*

En Java, un package (paquete en español) es una forma de organizar y agrupar un conjunto de clases, interfaces y otros elementos relacionados en un mismo espacio de nombres. Los paquetes ayudan a evitar conflictos de nombres entre diferentes clases y también hacen que el código sea más fácil de mantener y entender.

Para utilizar un paquete en Java, debes primero importarlo en tu código. Para hacerlo, se usa la palabra clave *import* seguida del nombre del paquete y la clase que quieres utilizar. Por ejemplo, si tienes un paquete llamado `com.example` que contiene una clase llamada `MiClase`, la importarías en tu código de la siguiente manera:

```java
import com.example.MiClase;
```

Una vez importada, puedes utilizar la clase en tu código de la misma forma que cualquier otra clase. Por ejemplo, podrías crear una instancia de la clase y llamar a un método en ella:

```java
MiClase objeto = new MiClase();
objeto.miMetodo();
```

También es posible importar todo el paquete completo, en lugar de una clase específica, utilizando un asterisco (*). Por ejemplo:
```java
import com.example.*;
```

Este código importaría todas las clases del paquete `com.example`. Sin embargo, esta práctica no es muy recomendable porque puede hacer que tu código sea más difícil de entender.

En resumen, los paquetes en Java son una forma útil de organizar y estructurar tu código. Al usarlos correctamente, puedes evitar conflictos de nombres y hacer que tu código sea más fácil de leer y mantener.


Para indicar que una clase pertenece a un paquete específico en Java, debes declarar el nombre del paquete al principio del archivo de código fuente de la clase utilizando la palabra clave *package*. Por ejemplo, si quieres que tu clase llamada `MiClase` pertenezca al paquete `com.example`, deberías colocar el siguiente código al principio del archivo:
```java
package com.example;

public class MiClase {
    // Código de la clase aquí
}
```

De esta manera, el compilador de Java sabrá que la clase `MiClase` pertenece al paquete `com.example` y podrá encontrarla cuando la importes en otros archivos de código fuente. Es importante que la declaración de paquete esté en la primera línea del archivo, antes de cualquier otra declaración de clase, interfaz, enumeración o cualquier otro tipo de entidad.

También debes asegurarte de que **la estructura de directorios de tu proyecto refleje la estructura de paquetes de tu código fuente**. En otras palabras, si tienes la clase `MiClase.java` en el paquete `com.example`, deberías guardarla en una carpeta llamada `com/example` en tu sistema de archivos, siendo la ruta al fichero así `com/example/MiClase.java`. De esta forma, el compilador de Java podrá encontrar las clases y paquetes correctamente.

En resumen, para indicar que una clase pertenece a un paquete en Java, debes declarar el nombre del paquete al principio del archivo de código fuente de la clase utilizando la palabra clave *package*, y asegurarte de que la estructura de directorios de tu proyecto refleje la estructura de paquetes de tu código fuente.