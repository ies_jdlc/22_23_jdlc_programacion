Herencia y Polimorfismo
===
Table of concepts:
- [Herencia y Polimorfismo](#herencia-y-polimorfismo)
  - [Herencia](#herencia)
    - [Resumen by Chat-GPT + Fer](#resumen-by-chat-gpt--fer)
    - [Resumen basado en el documento ud9\_POO2.pdf  del repositorio ApuntesProgramacion de lionel-ict](#resumen-basado-en-el-documento-ud9_poo2pdf--del-repositorio-apuntesprogramacion-de-lionel-ict)
      - [Constructores de clases derivadas](#constructores-de-clases-derivadas)
      - [Métodos heredados y sobreescritos](#métodos-heredados-y-sobreescritos)
      - [Clases y métodos final](#clases-y-métodos-final)
      - [Acceso a miembros derivados](#acceso-a-miembros-derivados)
  - [Polimorfismo](#polimorfismo)
    - [Resumen by Chat-GPT + Fer](#resumen-by-chat-gpt--fer-1)
    - [Resumen basado en el documento ud9\_POO2.pdf  del repositorio ApuntesProgramacion de lionel-ict](#resumen-basado-en-el-documento-ud9_poo2pdf--del-repositorio-apuntesprogramacion-de-lionel-ict-1)
  - [Clases y métodos abstractos](#clases-y-métodos-abstractos)
    - [Resumen by Chat-GPT + Fer](#resumen-by-chat-gpt--fer-2)
    - [Resumen basado en el documento ud9\_POO2.pdf  del repositorio ApuntesProgramacion de lionel-ict](#resumen-basado-en-el-documento-ud9_poo2pdf--del-repositorio-apuntesprogramacion-de-lionel-ict-2)
    - [Resumen clases y métodos abstractos](#resumen-clases-y-métodos-abstractos)
  - [Interfaz](#interfaz)
    - [Resumen by Chat-GPT + Fer](#resumen-by-chat-gpt--fer-3)
    - [Resumen basado en el documento ud9\_POO2.pdf  del repositorio ApuntesProgramacion de lionel-ict](#resumen-basado-en-el-documento-ud9_poo2pdf--del-repositorio-apuntesprogramacion-de-lionel-ict-3)
    - [Resumen Interfaz](#resumen-interfaz)


Documentación oficial sobre herencia e interfaces:
https://docs.oracle.com/javase/tutorial/java/IandI/index.html

## Herencia

### Resumen by Chat-GPT + Fer
La herencia es un mecanismo que permite a una clase heredar propiedades (atributos) y comportamientos (métodos) de otra clase existente, conocida como la clase padre o superclase. La clase que hereda se llama subclase o clase hija.

La herencia es una técnica fundamental de la programación orientada a objetos y es utilizada para reutilizar el código existente y crear jerarquías de clases relacionadas (la relación entre clases en la herencia es de *especialización* en la subclase y *generalización* en la superclase). La subclase hereda todos los atributos y todos los métodos, pero sólo tendrá acceso a los que estén declarados como *public*, *protected* o con modificador de acceso por defecto si la subclase y la superclase pertenecen al mismo *package*. Además, la subclase puede agregar nuevos atributos y métodos o sobrescribir los métodos heredados.

La sintaxis de la herencia en Java utiliza la palabra clave *extends*. Por ejemplo, si queremos crear una clase Empleado que hereda de la clase `Persona`, podemos escribir:
```java
public class Empleado extends Persona {
  // código de la clase Empleado
}
```
En este caso, la clase `Empleado` hereda todos los atributos y métodos de la clase Persona.

### Resumen basado en el documento [ud9_POO2.pdf](https://github.com/lionel-ict/ApuntesProgramacion/blob/master/Unidad%209%20POO%20(II)/ud9_POO2.pdf)  del repositorio [ApuntesProgramacion](https://github.com/lionel-ict/ApuntesProgramacion) de [lionel-ict](https://github.com/lionel-ict)

La herencia es una de las capacidades más importantes y distintivas de la POO. Consiste en derivar o extender una clase nueva a partir de otra ya existente de forma que la clase nueva hereda todos los atributos y métodos de la clase ya existente.

A la clase ya existente se la denomina superclase, clase base o clase padre. A la nueva clase se la denomina subclase, clase derivada o clase hija. 
Cuando derivamos (o extendemos) una nueva clase, ésta hereda todos los atributos y métodos miembro de la clase base. En Java se utiliza la palabra reservada *extends* para indicar herencia.


#### Constructores de clases derivadas
El constructor de una clase derivada debe encargarse de construir los atributos que estén definidos en la clase base además de sus propios atributos, pero la responsabilidad de escribir el constructor que inicializa los atributos suele y debe recaer en la clase que los declara.
Dentro del constructor de la clase derivada, para llamar al constructor de la clase base se debe utilizar el método reservado super() pasándole como argumento los parámetros que necesite.
Si no se llama explícitamente al constructor de la clase base mediante super() el compilador llamará automáticamente al constructor por defecto de la clase base. Si no tiene constructor por defecto el compilador generará un error.

#### Métodos heredados y sobreescritos
Hemos visto que una subclase hereda los atributos y métodos de la superclase; además, se pueden incluir nuevos atributos y nuevos métodos.
Por otro lado, puede ocurrir que alguno de los métodos que existen en la superclase no nos sirvan en la subclase (tal y como están programados) y necesitemos adecuarlos a las características de la subclase. Esto puede hacerse mediante la sobreescritura de métodos.

Un método está sobreescrito o reimplementado cuando se programa de nuevo en la clase derivada. El método sobreescrito en la clase derivada podría reutilizar el método de la clase base, si es necesario, y a continuación añadir el código específico necesarioe en la implementación de la subclase. En Java podemos acceder a método definidos en la clase base mediante `super.metodo()`.

####  Clases y métodos final
* Una clase final no puede ser heredada.
* Un método final no puede ser sobreescrito por las subclases.

#### Acceso a miembros derivados
Aunque una subclase incluye todos los miembros de su superclase, no podrá acceder a aquellos que hayan sido declarados como private.

También podemos declarar los atributos como proteted. De esta forma podrán ser accedidos desde las clases heredadas, (nunca desde otras clases).
Los atributos declarados como protected son públicos para las clases heredadas y privados para las demás clases.
## Polimorfismo

### Resumen by Chat-GPT + Fer

El polimorfismo se refiere a la capacidad de un objeto de una clase para tomar varias formas y comportarse de diferentes maneras en diferentes contextos. Esto significa que un objeto puede ser tratado (referenciado) como un objeto de su propia clase o como un objeto de cualquiera de sus clases padre o interfaces implementadas.

El polimorfismo en Java nos permite también hacer uso de la sobrescritura de métodos. La sobrescritura de métodos se refiere a la capacidad de una subclase para proporcionar su propia implementación de un método que ya está definido en su superclase. Esto permite que un objeto de la subclase use su propia versión del método en lugar de la versión de la superclase.

Por ejemplo, supongamos que tenemos una clase Animal con un método sonido() y dos subclases, Perro y Gato, que anulan el método sonido() de la clase Animal. Podemos crear un objeto Animal y tratarlo como un objeto Perro o Gato, y [dependiendo de la instancia que tengamos en tiempo de ejecución, se ejecutará el método correspondiente](https://es.wikipedia.org/wiki/Enlace_din%C3%A1mico_(programaci%C3%B3n_orientada_a_objetos)).
```java
Animal animal = new Perro();
animal.sonido(); // Se ejecutará el método sonido() de la clase Perro

animal = new Gato();
animal.sonido(); // Se ejecutará el método sonido() de la clase Gato
```
En resumen, el polimorfismo en Java nos permite escribir un código más genérico y reutilizable, ya que podemos tratar los objetos de diferentes clases de manera uniforme y permitir que el compilador determine qué método se ejecutará en tiempo de compilación o en tiempo de ejecución, dependiendo del contexto en el que se utilice.

### Resumen basado en el documento [ud9_POO2.pdf](https://github.com/lionel-ict/ApuntesProgramacion/blob/master/Unidad%209%20POO%20(II)/ud9_POO2.pdf)  del repositorio [ApuntesProgramacion](https://github.com/lionel-ict/ApuntesProgramacion) de [lionel-ict](https://github.com/lionel-ict)


La sobreescritura de métodos constituye la base de uno de los conceptos más potentes de Java: la [selección dinámica de métodos](https://es.wikipedia.org/wiki/Enlace_din%C3%A1mico_(programaci%C3%B3n_orientada_a_objetos)), que es un mecanismo mediante el cual la llamada a un método sobreescrito se resuelve en tiempo de ejecución y no durante la compilación. La selección dinámica de métodos es importante porque permite implementar el polimorfismo durante el tiempo de ejecución. Una variable de referencia a una superclase se puede referir a un objeto de cualquier subclase. Java se basa en esto para resolver llamadas a métodos sobreescritos en el tiempo de ejecución. 
**Lo que determina la versión del método que será ejecutado es el tipo de objeto al que se hace referencia y no el tipo de variable de referencia.**

El polimorfismo es fundamental en la programación orientada a objetos porque permite que una clase general especifique métodos que serán comunes a todas las clases que se deriven de esa misma clase. De esta manera las subclases podrán definir la implementación de alguno o de todos esos métodos. 
La superclase proporciona todos los elementos que una subclase puede usar directamente. También define aquellos métodos que las subclases que se deriven de ella deben implementar por sí mismas.  De esta manera, combinando la herencia y la sobreescritura de métodos, una superclase puede definir la forma general de los métodos que se usarán en todas sus subclases.


## Clases y métodos abstractos

### Resumen by Chat-GPT + Fer

Una clase abstracta es una clase que no puede ser instanciada directamente, sino que sirve como una plantilla para definir subclases concretas (no abstractas). 

Una clase abstracta se utiliza para definir un conjunto de métodos y propiedades (atributos) que deben ser implementados por las subclases, pero puede no proporcionarse la implementación para algunos de los métodos de la clase abstracta.
La principal diferencia entre una clase abstracta y una clase concreta es que una clase concreta se puede instanciar directamente, mientras que una clase abstracta no se puede instanciar. En lugar de eso, se utiliza como una plantilla para crear subclases concretas que proporcionan implementaciones para los métodos abstractos definidos en la clase abstracta. También se pueden sobrescribir métodos no abstractos.
Para crear una clase abstracta en Java, se utiliza la palabra clave *abstract* en la definición de la clase. 

Por ejemplo, supongamos que queremos crear una clase abstracta llamada Figura que define un método abstracto llamado calcularArea() que debe ser implementado por las subclases.
```java
public abstract class Figura {
    public abstract double calcularArea();
}
```

En este ejemplo, la clase Figura es una clase abstracta porque se ha definido utilizando la palabra clave abstract. También se ha definido un método abstracto llamado calcularArea(), que no tiene implementación en la clase abstracta. Esto significa que cualquier subclase de Figura debe proporcionar su propia implementación para el método calcularArea(), **de lo contrario también debe ser una clase abstracta**.

Es importante tener en cuenta que una clase abstracta puede tener métodos no abstractos y campos como cualquier otra clase. También puede tener constructores, pero no se puede utilizar para crear objetos de la clase abstracta directamente. En cambio, se debe utilizar para definir subclases concretas que proporcionen implementaciones para los métodos abstractos definidos en la clase abstracta.

### Resumen basado en el documento [ud9_POO2.pdf](https://github.com/lionel-ict/ApuntesProgramacion/blob/master/Unidad%209%20POO%20(II)/ud9_POO2.pdf)  del repositorio [ApuntesProgramacion](https://github.com/lionel-ict/ApuntesProgramacion) de [lionel-ict](https://github.com/lionel-ict)

Una clase abstracta es una clase que puede declarar la existencia de algunos métodos pero no su implementación (es decir, contiene la cabecera del método pero no su código). Los métodos sin implementar son métodos abstractos.
Una clase abstracta puede contener tanto métodos abstractos (sin implementar) como no abstractos (implementados). Para declarar una clase o método como abstracto se utiliza el modificador *abstract*.
Una clase abstracta no se puede instanciar, pero sí heredar. Las subclases tendrán que implementar obligatoriamente el código de los métodos abstractos (a no ser que también se declaren como abstractas).

Las clases abstractas son útiles cuando necesitemos definir una forma generalizada de clase que será compartida por las subclases, dejando parte del código en la clase abstracta (métodos “normales”) y delegando otra parte en las subclases (métodos abstractos).

No pueden declararse constructores o métodos estáticos abstractos.

La finalidad principal de una clase abstracta es crear una clase heredada a partir de ella. Por ello, en la práctica es obligatorio aplicar herencia (si no, la clase abstracta no sirve para nda). El caso contrario es una clase final, que no puede heredarse como ya hemos visto. Por lo tanto una clase no puede ser abstract y final al mismo tiempo.

### Resumen clases y métodos abstractos
* Si una clase tiene un método abstracto, la clase tiene que declararse como *abstract*
* Un método es abstracto cuando se indica con el modificador *abstract* y no tiene cuerpo: después de cerrar los paréntesis con la lista de parámetros se finaliza la línea con punto y coma `;` 
* Si una clase se declara como abstract, no se puede instanciar, no se pueden crear objetos de esa clase
* Si una clase **NO** es abstract, no puede tener ningún método abstract, todos lo métodos tienen que tener una implementación (cuerpo)


## Interfaz

### Resumen by Chat-GPT + Fer

En Java, una interfaz es un tipo de clase que define un conjunto de métodos abstractos (sin implementación) que otra clase que implemente la interfaz debe implementar. Una interfaz también puede definir constantes (atributos final) y métodos con una implementación predeterminada (*default*).

Las interfaces se utilizan para establecer un contrato entre dos partes: la interfaz y la clase que la implementa. Cuando una clase implementa una interfaz, está acordando proporcionar una implementación para todos los métodos definidos en la interfaz.
Las interfaces son útiles porque permiten que una clase implemente múltiples interfaces, lo que le permite cumplir diferentes contratos con diferentes partes de un programa. También permiten la creación de código más modular y extensible, ya que los programadores pueden diseñar y escribir interfaces sin tener que preocuparse por el código de la implementación.

Además, las interfaces también se utilizan para lograr la abstracción y el polimorfismo en Java, lo que significa que se pueden escribir programas que se adapten a diferentes tipos de objetos sin conocer su clase exacta, sino solo la interfaz que implementan. Esto permite una mayor flexibilidad en el diseño y la implementación de sistemas de software complejos.

### Resumen basado en el documento [ud9_POO2.pdf](https://github.com/lionel-ict/ApuntesProgramacion/blob/master/Unidad%209%20POO%20(II)/ud9_POO2.pdf)  del repositorio [ApuntesProgramacion](https://github.com/lionel-ict/ApuntesProgramacion) de [lionel-ict](https://github.com/lionel-ict)

Una interfaz es una declaración de atributos y métodos sin implementación (sin definir el código de los métodos). Se utilizan para definir el conjunto mínimo de atributos y métodos de las clases que implementen dicha interfaz. En cierto modo, es parecido a una clase abstracta con todos sus miembros abstractos.

**Si una clase es una plantilla para crear objetos, una interfaz es una pantilla para crear clases.**

Mediante la construcción de un interfaz, el programador pretende especificar qué caracteriza a una colección de objetos e, igualmente, especificar qué comportamiento deben reunir los objetos que quieran entrar dentro de sea categoría o colección.
En una interfaz también se pueden declarar constantes que definen el comportamiento que deben soportar los objetos que quieran implementar esa interfaz.

La sintaxis típica de una interfaz es la siguiente:
```java
public interface Nombre {
	// Declaración de atributos y métodos (sin definir código)
}
```

Si una interfaz define un tipo pero ese tipo no provee de ningún método, podemos preguntarnos: ¿para qué sirven entonces las interfaces en Java? 
La implementación (herencia) de una interfaz no podemos decir que evite la duplicidad de código o que favorezca la reutilización de código puesto que realmente no proveen código.

En cambio sí podemos decir que reúne las otras dos ventajas de la herencia: favorecer el mantenimiento y la extensión de las aplicaciones. ¿Por qué? Porque al definir interfaces permitimos la existencia de variables polimórficas y la invocación polimórfica de métodos.

Un aspecto fundamental de las interfaces en Java es separar la especificación de una clase (qué hace) de la implementación (cómo lo hace). Esto se ha comprobado que da lugar a programas más robustos y con menos errores. 

Es importante tener en cuenta que:
* Una interfaz no se puede instanciar en objetos, solo sirve para implementar clases
* Una clase puede implementar varias interfaces (separadas por comas)
* Una clase que implementa una interfaz debe proporcionar la implementación para todos y cada uno de los métodos definidos en la interfaz, o bien declararse como clase abstracta
* Las clases que implementan una interfaz que tiene definidas constantes pueden usarlas en cualquier parte del código de la clase, simplemente indicando su nombre

### Resumen Interfaz
* Las interfaces, al igual que las clases abstractas, no se pueden utilizar para crear objetos
* Los métodos de una interfaz no tienen cuerpo/implementación, excepto cuando se especifica una implementación por defecto con el modificador *default*
* Los métodos de una interfaz son por defecto *public* y *abstract* 
* Los atributos de una interfaz son por defecto *public*, *static* y *final*, es decir, son constantes públicas que pertenecen a la clase.
