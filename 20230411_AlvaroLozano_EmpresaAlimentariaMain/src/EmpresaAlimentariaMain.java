import programacion.empresa.Producto;
import programacion.empresa.ProductoFresco;
import programacion.empresa.ProductoRefrigerado;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class EmpresaAlimentariaMain {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    ArrayList<ProductoFresco> arrayProductoFresco = new ArrayList<ProductoFresco>();
    ArrayList<ProductoRefrigerado> arrayProductoRefrigerado = new ArrayList<ProductoRefrigerado>();
    arrayProductoFresco.add(new ProductoFresco("lechuga", "2023-04-10", 70000, "ES", "2023-06-30"));
    arrayProductoFresco.add(new ProductoFresco("pepino", "2023-04-11", 60000, "FR", "2023-06-21"));

    arrayProductoRefrigerado.add(new ProductoRefrigerado("yogur", 2000, 0, "GR", "2024-01-01", 66666));

    // Variables auxiliares para leer con el scanner
    String nombre;
    String fechaDeCaducidad;
    int numeroDeLote;
    String fechaEnvasado;
    String paisDeOrigen;
    int temperaturaRecomendada;

    ProductoRefrigerado productoRefrigerado1 = new ProductoRefrigerado();

    // REALIZO EL MENU PARA PEDIR QUE PRODUCTO QUIERE INCLUIR
    int opcion;
    do {
      Menu();
      opcion = in.nextInt();
      in.nextLine();
      switch (opcion) {
        case 1:
          System.out.println("Has seleccionado introducir un producto fresco.");
          ProductoFresco producto1 = new ProductoFresco();

          // PIDO INTRODUCIR LOS DATOS DEL PRODUCTO
          do {
            System.out.println("Introduce el nombre del producto:");
            nombre = in.nextLine();
            System.out.println("Introduce su fecha de caducidad:");
            fechaDeCaducidad = in.nextLine();
            System.out.println("Introduce su numero de lote: ");
            numeroDeLote = in.nextInt();
            in.nextLine();
            producto1.setDatos(nombre, fechaDeCaducidad, numeroDeLote);

            // COMPRUEBO QUE LOS DATOS SON CORRECTOS
            if (producto1.datosCorrectos() == false) {
              System.out.println("Los datos que has introducido no son correctos. Vuelve a introducirlos.");
            }
          } while (producto1.datosCorrectos() == false);

          System.out.println("Introduce la fecha de envasado del producto:");
          fechaEnvasado = in.nextLine();
          LocalDate localDateFechaEnvasado = LocalDate.parse(fechaEnvasado);

          System.out.println("Introduce el pais de origen del producto:");
          paisDeOrigen = in.nextLine();

          producto1.setDatosProductoFresco(localDateFechaEnvasado, paisDeOrigen);
          System.out.println("           ");
          System.out.println(producto1);
          System.out.println("-----------------");
          break;

        case 2:
          do {
            System.out.println("Has seleccionado introducir un producto refrigerado.");
            System.out.println("Introduce el nombre del producto:");
            nombre = in.nextLine();
            System.out.println("Introduce su fecha de caducidad:");
            fechaDeCaducidad = in.nextLine();
            System.out.println("Introduce su numero de lote: ");
            numeroDeLote = in.nextInt();
            in.nextLine();
            System.out.println("Introduce el codigo del organismo que envia el producto.");
            int codigoOrganismo;
            codigoOrganismo = in.nextInt();
            in.nextLine();
            System.out.println("Introduce la temperatura recomendada del producto:");
            temperaturaRecomendada = in.nextInt();
            in.nextLine();
            System.out.println("Introduce el pais de origen del producto:");
            paisDeOrigen = in.nextLine();

            productoRefrigerado1.setDatos(nombre, fechaDeCaducidad, numeroDeLote);
            productoRefrigerado1.setDatosProductoRefrigerado(codigoOrganismo, temperaturaRecomendada, paisDeOrigen);

            if (productoRefrigerado1.datosCorrectos() == false) {
              System.out.println("Datos introducidos incorrectos.");
            }
          } while (productoRefrigerado1.datosCorrectos() == false);

          System.out.println();
          System.out.println(productoRefrigerado1);
          System.out.println("*******************");

          // for (int i = 0; i < arrayProductoRefrigerado.size(); i++) {
          // if (arrayProductoRefrigerado.get(i) == null) {
          arrayProductoRefrigerado.add(productoRefrigerado1);
          // }
          // }

          System.out.println("¿Desea ver los productos refrigerados guardados? S/N");
          String respuesta;
          respuesta = in.nextLine();
          if (respuesta.equalsIgnoreCase("S")) {
            for (int i = 0; i < arrayProductoRefrigerado.size(); i++) {
              System.out.println((i+1) + ") " + arrayProductoRefrigerado.get(i));
            }
            for(ProductoRefrigerado variableIteracion: arrayProductoRefrigerado){
              System.out.println(variableIteracion);
            }

          } else if (respuesta.equalsIgnoreCase("N")) {
            System.out.println("Has decidido no ver los productos almacenados.");
          } else {
            System.out.println("Respuesta introducida no reconocida.");
          }
          break;

        case 3:
          break;
        case 4:
          System.out.println("Goodbye!");
          break;
        default:
          System.out.println("La opcion ingresada no es correcta. Vuelve a intentarlo.");
          System.out.println();
          break;
      }
      System.out.println("==> Productos Refrigerados ("+ arrayProductoRefrigerado.size() + "):");
      for(ProductoRefrigerado variableIteracion: arrayProductoRefrigerado){
        System.out.println(variableIteracion);
      }
      System.out.println("==> Productos Frescos ("+ arrayProductoFresco.size() + "):");
      for(Producto variableIteracion: arrayProductoFresco){
        System.out.println(variableIteracion);
      }

    } while (opcion != 4);
  }

  public static void Menu() {
    System.out.println("***MENU EMPRESA CONGELADOS***\n" +
        "Elige una opcion:\n" +
        "1. Ingresar producto fresco.\n" +
        "2. Ingresar producto refrigerado.\n" +
        "3. Ingresar producto congelado.\n" +
        "4. SALIR.......");
  }
}
