package programacion.empresa;

public class ProductosCongelados extends Producto {
    String paisDeOrigen;
    int temperaturaRecomendada;

    public ProductosCongelados(String paisDeOrigen, int temperaturaRecomendada, String fechaDeCaducidad, int numeroDeLote){
        super(paisDeOrigen, fechaDeCaducidad, numeroDeLote);
        this.paisDeOrigen = paisDeOrigen;
        this.temperaturaRecomendada = temperaturaRecomendada;
    }

    public String toString(){
        return super.toString() + ", " + paisDeOrigen + ", " + temperaturaRecomendada;
    }
}

