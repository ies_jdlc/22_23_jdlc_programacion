package programacion.empresa;

public class congeladosNitrogeno extends ProductosCongelados {
    String metodoCongelacion;
    int tiempoExposicionNitrogeno;

    public congeladosNitrogeno(String metodoCongelacion, int tiempoExposicionNitrogeno, String fechaEnvasado, String paisDeOrigen, int temperaturaRecomendada, String fechaDeCaducidad, int numeroDeLote){
        super(paisDeOrigen, temperaturaRecomendada, fechaDeCaducidad, numeroDeLote);
        this.metodoCongelacion = metodoCongelacion;
        this.tiempoExposicionNitrogeno = tiempoExposicionNitrogeno;
    }
}
