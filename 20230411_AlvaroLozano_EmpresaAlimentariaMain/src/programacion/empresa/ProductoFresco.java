package programacion.empresa;

import java.time.LocalDate;

public class ProductoFresco extends Producto {
  String paisDeOrigen;
  LocalDate fechaEnvasado;

  public ProductoFresco(String nombre, String fechaDeCaducidad, int numeroDeLote, String paisDeOrigen,
      String fechaEnvasado) {
    super(nombre, fechaDeCaducidad, numeroDeLote);
    this.paisDeOrigen = paisDeOrigen;
    this.fechaEnvasado = LocalDate.parse(fechaEnvasado);
  }

  public ProductoFresco() {

  }

  public void setDatosProductoFresco(LocalDate fechaEnvasado, String paisDeOrigen) {
    this.fechaEnvasado = fechaEnvasado;
    this.paisDeOrigen = paisDeOrigen;
  }

  public String toString() {
    return super.toString() + ", " + paisDeOrigen + ", " + fechaEnvasado;
  }
}
