package programacion.empresa;

public class ProductoRefrigerado extends Producto {
    int codigoOrganismo;
    int temperaturaRecomendada;
    String paisDeOrigen;

    public ProductoRefrigerado(String nombre, int codigoOrganismo, int temperaturaRecomendada, String paisDeOrigen, String fechaDeCaducidad, int numeroDeLote){
        super(nombre, fechaDeCaducidad, numeroDeLote);
        this.codigoOrganismo = codigoOrganismo;
        this.temperaturaRecomendada = temperaturaRecomendada;
        this.paisDeOrigen = paisDeOrigen;
    }

    public ProductoRefrigerado(){
        
    }

    public String toString(){
        return super.toString() + ", " +  codigoOrganismo + ", "+  temperaturaRecomendada + ", " +  paisDeOrigen;
    }

    public void setDatosProductoRefrigerado (int codigoOrganismo, int temperaturaRecomendada, String paisDeOrigen){
        this.codigoOrganismo = codigoOrganismo;
        this.temperaturaRecomendada = temperaturaRecomendada;
        this.paisDeOrigen = paisDeOrigen;
    }

    public boolean datosCorrectos(){

        if(super.nombre.isEmpty()){
            return false;
        }

        if(super.fechaDeCaducidad.isEmpty()){
            return false;
        }

        if(super.numeroDeLote < 50000 || super.numeroDeLote > 99999){
            return false;
        }

        if (codigoOrganismo < 1000 || codigoOrganismo > 7000){
            return false;
        }

        if (temperaturaRecomendada > 10){
            System.out.println("Los productos refrigerados no pueden almacenarse a mas de 10ºC.");
            return false;
        }

        if (paisDeOrigen.isEmpty()){
            return false;
        }

        return true;
    }
}
