package programacion.empresa;

public abstract class Producto {
  String nombre;
  String fechaDeCaducidad;
  int numeroDeLote;

  public Producto() {

  }

  public Producto(String nombre, String fechaDeCaducidad, int numeroDeLote) {
    this.nombre = nombre;
    this.fechaDeCaducidad = fechaDeCaducidad;
    this.numeroDeLote = numeroDeLote;
  }

  public void setDatos(String nombre, String fechaDeCaducidad, int numeroDeLote) {
    this.nombre = nombre;
    this.fechaDeCaducidad = fechaDeCaducidad;
    this.numeroDeLote = numeroDeLote;
  }

  public boolean datosCorrectos() {
    if (nombre.isEmpty()) {
      return false;
    }

    if (fechaDeCaducidad.isEmpty()) {
      return false;
    }

    if (numeroDeLote < 50000 || numeroDeLote > 99999) {
      return false;
    }

    return true;
  }

  public String toString() {
    return nombre + ", " + fechaDeCaducidad + ", " + numeroDeLote;
  }
}
