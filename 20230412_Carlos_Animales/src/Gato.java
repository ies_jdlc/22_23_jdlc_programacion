public class Gato extends Animal {
  public Gato(String nombre, String especie) {
    super(nombre, especie);
  }
  @Override
  public void hacerRuido(){
    System.out.println("Miau");
  }
  public void respirar() {
    System.out.println("Soy un gato y respiro");
  }
}
