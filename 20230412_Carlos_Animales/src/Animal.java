public abstract class Animal implements SerVivo {
  String nombre;
  String especie;

  
  public Animal(String nombre, String especie) {
    this.nombre = nombre;
    this.especie = especie;
  }


  @Override
  public String toString() {
    return "Soy un " + especie + " y me llamo " + nombre;
  }

  
}
