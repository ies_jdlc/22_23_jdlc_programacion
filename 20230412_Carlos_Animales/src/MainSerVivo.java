import java.util.ArrayList;
import java.util.List;

public class MainSerVivo {
  public static void main(String[] args) {
    Gato gato = new Gato("Garfield", "gato");
    Perro perro = new Perro("milu", "perro");

    List<Animal> animales = new ArrayList<Animal>();
    animales.add(gato);
    animales.add(perro);

    for(Animal iterador : animales){
      System.out.println(iterador);
      iterador.respirar();
      iterador.hacerRuido();
    }


    /*System.out.println(gato);
    gato.respirar();
    gato.hacerRuido();
    
    System.out.println(perro);
    perro.respirar();
    perro.hacerRuido();*/


  }
}
