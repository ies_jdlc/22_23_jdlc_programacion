public class Perro extends Animal {
  public Perro(String nombre, String especie) {
    super(nombre, especie);
  }
  @Override
  public void hacerRuido(){
    System.out.println("Guau");
  }

  public void respirar(){
    System.out.println("Soy un perro y respiro");
  }
}
