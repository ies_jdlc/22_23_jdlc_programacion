import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class MainDuplicadosSet {
  static final int MAX_VALUE = 3;
  static final int NUM_REPETICIONES = 2;

  public static void main(String[] args) {

    // Explicación de porqué no hace falta pasar el tipo contenido en la LinkedList
    // llamando al constructor con 'new LinkedList<Integer>();' y porqué 'new
    // LinkedList<>();' es suficiente:
    //
    // Inferencia de tipo con operador diamante <>:
    // https://www.arquitecturajava.com/java-var-keyword-y-su-uso-con-jdk10/
    // https://javadesdecero.es/avanzado/inferencia-tipo-operador-diamante-java/
    // https://docs.oracle.com/javase/tutorial/java/generics/types.html#diamond

    List<Integer> listaIntegers = new LinkedList<>();
    Queue<Integer> colaIntegers = new LinkedList<>();
    Set<Integer> setIntegers = new LinkedHashSet<Integer>();
    /*Es lo mismo: 
     *  
    var setIntegers2 = new TreeSet<Integer>();
    TreeSet<Integer> setIntegers3 = new TreeSet<Integer>();
     */

  

    /*try {
      setIntegers.add("Ejemplo");
      
    } catch (ClassCastException e) {
      // TODO: handle exception
      System.out.println("Debería ser integer");
    }*/

    if(setIntegers.add(4) == false){
      System.out.println("No funciona el primero");
    }
    if(!setIntegers.add(5)){
      System.out.println("No funciona el segundo");
    }
   
    var it = setIntegers.iterator();

    //Iterator<Integer> it = setIntegers.iterator();
    System.out.println("imprimimos el set: ");

    while(it.hasNext()){
      System.out.println(it.next());
    }
    if(setIntegers.add(300) == false){
      System.out.println("No funciona el tercero");
    }
    if(setIntegers.add(66) == false){
      System.out.println("No funciona el cuarto");
    }
    if(setIntegers.add(4) == false){
      System.out.println("No funciona el quinto add(4)");
    }

    it = setIntegers.iterator();
    System.out.println("imprimimos el set: ");
    while(it.hasNext()){
      System.out.println(it.next());
    }

  }
}
