package ejemplo6enumerados;

public class UsoEnumerados {

  public static void main(String[] args) {
    Talla t = Talla.PEQUEÑA;
    Demarcacion puesto = Demarcacion.DEFENSA;
    Demarcacion puesto2 = Demarcacion.PORTERO;

    System.out.println("Qué visualiza el método toString(): " + t.toString());
    System.out.println("Qué visualiza el método ordinal(): " + t.ordinal());
    System.out.println("Qué visualiza el método comparteTo(): " + puesto2.compareTo(puesto));

    System.out.println("===> Jugando con DiaDeLaSemana:");
    DiaDeLaSemana dia1 = DiaDeLaSemana.LUNES;
    DiaDeLaSemana dia2 = DiaDeLaSemana.MARTES;
    DiaDeLaSemana dia3 = DiaDeLaSemana.DOMINGO;

    System.out.println("dia1=DiaDeLaSemana.LUNES;   dia1.toString():" + dia1.toString() + ";   dia1.ordinal():" + dia1.ordinal());
    System.out.println("dia2=DiaDeLaSemana.MARTES;  dia2.toString():" + dia2.toString() + ";  dia2.ordinal():" + dia2.ordinal());
    System.out.println("dia3=DiaDeLaSemana.DOMINGO; dia3.toString():" + dia3.toString() + "; dia3.ordinal():" + dia3.ordinal());
    System.out.println("COMPARACIONES:");
    System.out.println("dia2.compareTo(dia1)=" + dia2.compareTo(dia1));
    System.out.println("dia2.compareTo(dia3)=" + dia2.compareTo(dia3));

  }

  public enum Talla {
    PEQUEÑA, MEDIANA, GRANDE
  };

  public enum Demarcacion {
    PORTERO, DEFENSA, DELANTERO, CENTROCAMPISTA
  };
}
