package ejemplo4clasesabstract;

public class Derivada extends ClaseAbstracta1 {

  public Derivada() {
  }

  public Derivada(String n) {
    super.nombrePersona = n;
    // También valdría nombrePersona sin usar super. No hay ambigüedad, el atributo
    // nombrePersona sólo está declarado en ClaseAbstracta1.
    // nombrePersona = n;
  }

  // Las clases que heredan de ClaseAbstracta1 tienen que implementar sus métodos
  // abstractos, en este caso 'saludar()'. Si no lo implementa, la clase Derivada
  // tendría que ser abstract también.
  @Override
  public void saludar() {
    System.out.println("Hola qué tal estás " + nombrePersona);
  }

}
