public class Coche {
  String marca;
  String modelo;
  float largo;

  static final String AUDI = "audi";
  static final String BMW = "bmw";
  static final String MERCEDES = "mercedes";

  public Coche(String marca, String modelo, float largo) {
    this.marca = marca;
    this.modelo = modelo;
    this.largo = largo;
  }

  public int pasarRevision() {
    switch (marca.toLowerCase()) {
      case AUDI:
        return 5;
      case BMW:
        return 6;
      case MERCEDES:
        return 7;
      default:
        return 3;
    }
  }

}
