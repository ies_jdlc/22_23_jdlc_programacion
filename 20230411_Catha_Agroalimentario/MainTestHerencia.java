import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import programacion.empresaAgroalimentaria.Producto;
import programacion.empresaAgroalimentaria.ProductosCongeladosAgua;
import programacion.empresaAgroalimentaria.ProductosCongeladosAire;
import programacion.empresaAgroalimentaria.ProductosCongeladosNitrogeno;
import programacion.empresaAgroalimentaria.ProductoFresco;
import programacion.empresaAgroalimentaria.ProductosRefrigerados;


public class MainTestHerencia {
    public static void main(String[] args) {
      
        Scanner in = new Scanner(System.in);
        // System.out.println("Introduzca el nombre del producto: ");
        // String nombreProducto=in.nextLine();
        in.close();
        List<Producto> listaGeneral = new ArrayList<>();
        
        Producto productoAuxiliar = new ProductoFresco(2022, 05, 27, "P3C5", 2022, 05, 20, "España");
        listaGeneral.add(productoAuxiliar);
        productoAuxiliar = new ProductoFresco(2022, 05, 25, "A352", 2022, 05, 20, "España");
        listaGeneral.add(productoAuxiliar);

        productoAuxiliar = new ProductosRefrigerados(2022, 05, 21, "A456SD", "BFr63547", 2022, 05, 15, 15.22f,"Alemania");
        listaGeneral.add(productoAuxiliar);

        ProductosRefrigerados producto4 = new ProductosRefrigerados(2022, 05, 26, "A5645SD", "BFr656547", 2022, 05, 15, 25.12f,"Alemania");
        listaGeneral.add(producto4);

        ProductosRefrigerados producto5 = new ProductosRefrigerados(2022, 05, 27, "A54778F", "BFr6589s7", 2022, 05, 10, 20.3f,"Francia");
        listaGeneral.add(producto5);

        ProductosCongeladosAgua producto6 = new ProductosCongeladosAgua(2023, 06, 10, "A1254", 2023, 3, 20, "Italia", 20f,10.2f);
        listaGeneral.add(producto6);

        ProductosCongeladosAgua producto7 = new ProductosCongeladosAgua(2023, 06, 15, "B54754", 2023, 3, 19, "Italia", 21.2f, 11.7f);
        listaGeneral.add(producto7);


        ProductosCongeladosAire producto8 = new ProductosCongeladosAire(2023, 10, 20, "U4568KJ", 2023, 9, 20, "Turquia", 15.5f, 20, 30, 40);
        listaGeneral.add(producto8);
        
        ProductosCongeladosAire producto9 = new ProductosCongeladosAire(2023, 10, 20, "U4568KJ", 2023, 9, 20, "Turquia", 15.5f, 41, 19, 40);
        listaGeneral.add(producto9);

        System.out.println("===> listaGeneral:");
        for(Producto producto: listaGeneral){
          System.out.println(producto);
        }
        System.out.println("===============");

        
        ProductosCongeladosNitrogeno producto10 = new ProductosCongeladosNitrogeno(2022, 01, 25, "ART5478", 2021, 8, 8, "Portugal", 15.7f, "Envasado al vacío", 120);

        //System.out.println("Productos frescos:\nProducto 1: " + productoAuxiliar + "\nProducto 2: "+producto2);

        //System.out.println("Productos refrigerados:\nProducto 1: " + producto3 + "\nProducto 2: "+producto4+ "\nProducto 3: "+producto5);

        System.out.println("Productos Congelados Agua:\nProducto 1: " + producto6 + "\nProducto 2: "+producto7);

        System.out.println("Productos Congelados Aire:\nProducto 1: " + producto8 + "\nProducto 2: "+producto9);

        System.out.println("Productos Congelados Aire:\nProducto 1: " + producto10 + "\n");
    }

}
