package programacion.empresaAgroalimentaria;

public class ProductosCongeladosAire extends ProductosCongelados {

    private float composicionNitrogeno;
    private float composicionOxigeno;
    private float composicionVaporAgua;

    public ProductosCongeladosAire(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroSerie,
            int yearEnvasado, int monthEnvasado, int dayEnvasado, String paisOrigen,
            float tempRecom, float composicionNitrogeno, float composicionOxigeno, float composicionVaporAgua) {
        super(yearCaducidad, monthCaducidad, dayCaducidad, numeroSerie, yearEnvasado, monthEnvasado, dayEnvasado,
                paisOrigen, tempRecom);
        this.composicionNitrogeno = composicionNitrogeno;
        this.composicionOxigeno = composicionOxigeno;
        this.composicionVaporAgua = composicionVaporAgua;
    }

    public float getComposicionNitrogeno() {
        return composicionNitrogeno;
    }

    public void setComposicionNitrogeno(float composicionNitrogeno) {
        this.composicionNitrogeno = composicionNitrogeno;
    }

    public float getComposicionOxigeno() {
        return composicionOxigeno;
    }

    public void setComposicionOxigeno(float composicionOxigeno) {
        this.composicionOxigeno = composicionOxigeno;
    }

    public float getComposicionVaporAgua() {
        return composicionVaporAgua;
    }

    public void setComposicionVaporAgua(float composicionVaporAgua) {
        this.composicionVaporAgua = composicionVaporAgua;
    }

    public String toString() {
        return "El producto COngelado por Aire contiene una composición de:\n- % de Nitrógeno: " + composicionNitrogeno
                + "\n- % de Oxígeno: "
                + composicionOxigeno + "\n- % de Vapor de Agua: " + composicionVaporAgua+"\n";
    }

}
