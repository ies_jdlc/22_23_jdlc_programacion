package programacion.empresaAgroalimentaria;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class ProductosCongelados extends Producto {
    private Date fechaEnvasado;
    private String paisOrigen;
    private float tempRecom;

    public ProductosCongelados(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroSerie,
            int yearEnvasado, int monthEnvasado, int dayEnvasado, String paisOrigen,
            float tempRecom) {
        super(yearCaducidad, monthCaducidad, dayCaducidad, numeroSerie);
        GregorianCalendar fechaNueva = new GregorianCalendar(yearEnvasado, monthEnvasado, dayEnvasado);
        this.fechaEnvasado = fechaNueva.getTime();
        this.paisOrigen = paisOrigen;
        this.tempRecom = tempRecom;
    }

    public Date getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(int yearEnvasado, int monthEnvasado, int dayEnvasado) {
        GregorianCalendar fechaNueva = new GregorianCalendar(yearEnvasado, monthEnvasado, dayEnvasado);
        this.fechaEnvasado = fechaNueva.getTime();
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public float getTempRecom() {
        return tempRecom;
    }

    public void setTempRecom(float tempRecom) {
        this.tempRecom = tempRecom;
    }

    public String toString() {
        return "El producto Congelado tiene una fecha de envasado: " + new SimpleDateFormat("dd-MM-yyyy").format(fechaEnvasado) + ", su país de origen es: "
                + paisOrigen + ", siendo la temperatura de mantenimiento recomendada: "
                + tempRecom + ".\n";
    }

}
