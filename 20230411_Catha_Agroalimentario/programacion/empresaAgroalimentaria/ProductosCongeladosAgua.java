package programacion.empresaAgroalimentaria;

public class ProductosCongeladosAgua extends ProductosCongelados {

    private float salinidadAgua; //gramos de sal por libro de agua

    public ProductosCongeladosAgua(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroSerie, int yearEnvasado, int monthEnvasado, int dayEnvasado, String paisOrigen,
            float tempRecom, float salinidadAgua) {
        super(yearCaducidad, monthCaducidad, dayCaducidad, numeroSerie, yearEnvasado, monthEnvasado, dayEnvasado, paisOrigen, tempRecom);
        this.salinidadAgua=salinidadAgua;
    }

    public float getSalinidadAgua() {
        return salinidadAgua;
    }

    public void setSalinidadAgua(float salinidadAgua) {
        this.salinidadAgua = salinidadAgua;
    }

    
    public String toString() {
        return "El producto Congelado por Agua tiene una salinidad de: " + salinidadAgua + ".\n";
    }
    
}
