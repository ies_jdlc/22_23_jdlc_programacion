package programacion.empresaAgroalimentaria;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class Producto {
    private Date fechaCaducidad;
    private String numeroLote;

    public Producto(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroLote) {
        GregorianCalendar fecha=new GregorianCalendar(yearCaducidad, monthCaducidad, dayCaducidad);
        this.fechaCaducidad = fecha.getTime();
        this.numeroLote = numeroLote;
    }

    public Date getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(int year, int month, int day) {
        GregorianCalendar fechanueva=new GregorianCalendar(year, month, day);
        fechaCaducidad = fechanueva.getTime();
    }

    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String toString() {
        return "El producto tiene una fecha de caducidad de: " + new SimpleDateFormat("dd-MM-yyyy").format(fechaCaducidad) + "y un numero de lote: " + numeroLote+".";
    }

}
