package programacion.empresaAgroalimentaria;

public class ProductosCongeladosNitrogeno extends ProductosCongelados {

    private String infoMetodoCongelacion;
    private int tiempoExposicionNitrogeno;

    public ProductosCongeladosNitrogeno(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroSerie, int yearEnvasado, int monthEnvasado, int dayEnvasado, String paisOrigen,
            float tempRecom, String infoMetodoCongelacion, int tiempoExposicionNitrogeno) {
        super(yearCaducidad, monthCaducidad, dayCaducidad, numeroSerie, yearEnvasado, monthEnvasado, dayEnvasado, paisOrigen, tempRecom);
        this.infoMetodoCongelacion=infoMetodoCongelacion;
        this.tiempoExposicionNitrogeno=tiempoExposicionNitrogeno;
    }

    public String getInfoMetodoCongelacion() {
        return infoMetodoCongelacion;
    }

    public void setInfoMetodoCongelacion(String infoMetodoCongelacion) {
        this.infoMetodoCongelacion = infoMetodoCongelacion;
    }

    public int getTiempoExposicionNitrogeno() {
        return tiempoExposicionNitrogeno;
    }

    public void setTiempoExposicionNitrogeno(int tiempoExposicionNitrogeno) {
        this.tiempoExposicionNitrogeno = tiempoExposicionNitrogeno;
    }

    public String toString() {
        return "El producto congelado por nitrógeno posee la siguiente información en relación al método de congelación: " + infoMetodoCongelacion
                + ".\nEl tiempo de exposición a nitrógeno es: " + tiempoExposicionNitrogeno + "segundos.\n";
    }

    
    
}
