package programacion.empresaAgroalimentaria;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class ProductoFresco extends Producto {
    private Date fechaEnvasado;
    private String paisOrigen;

    public ProductoFresco(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroSerie, int yearEnvasado, int monthEnvasado, int dayEnvasado, String paisOrigen) {
        super(yearCaducidad, monthCaducidad, dayCaducidad, numeroSerie);
        GregorianCalendar fechaNueva = new GregorianCalendar(yearEnvasado, monthEnvasado, dayEnvasado);
        this.fechaEnvasado = fechaNueva.getTime();
        this.paisOrigen = paisOrigen;

    }

    public Date getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(int yearEnvasado, int monthEnvasado, int dayEnvasado) {
        GregorianCalendar nuevaFecha = new GregorianCalendar(yearEnvasado, monthEnvasado, dayEnvasado);
        this.fechaEnvasado = nuevaFecha.getTime();
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String toString() {
        return "El producto fresco tiene una fecha de envasado: " + new SimpleDateFormat("dd-MM-yyyy").format(fechaEnvasado) + ", y un país de origen: " + paisOrigen + ".\n";
    }

}
