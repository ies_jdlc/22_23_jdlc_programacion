package programacion.empresaAgroalimentaria;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class ProductosRefrigerados extends Producto {

    private String codOSA;
    private Date fechaEnvasado;
    private float tempRecom;
    private String paisOrigen;

    public ProductosRefrigerados(int yearCaducidad, int monthCaducidad, int dayCaducidad, String numeroSerie,
            String codOSA, int yearEnvasado, int monthEnvasado, int dayEnvasado,
            float tempRecom, String paisOrigen) {
        super(yearCaducidad, monthCaducidad, dayCaducidad, numeroSerie);
        this.codOSA = codOSA;
        GregorianCalendar fecha = new GregorianCalendar(yearEnvasado, monthEnvasado, dayEnvasado);
        this.fechaEnvasado = fecha.getTime();
        this.tempRecom = tempRecom;
        this.paisOrigen = paisOrigen;
    }

    public String getCodOSA() {
        return codOSA;
    }

    public void setCodOSA(String codOSA) {
        this.codOSA = codOSA;
    }

    public Date getFechaEnvasado() {
        return fechaEnvasado;
    }

    public void setFechaEnvasado(int yearEnvasado, int monthEnvasado, int dayEnvasado) {
        GregorianCalendar fechaNueva = new GregorianCalendar(yearEnvasado, monthEnvasado, dayEnvasado);
        this.fechaEnvasado = fechaNueva.getTime();
    }

    public float getTempRecom() {
        return tempRecom;
    }

    public void setTempRecom(float tempRecom) {
        this.tempRecom = tempRecom;
    }

    public String getPaisOrigen() {
        return paisOrigen;
    }

    public void setPaisOrigen(String paisOrigen) {
        this.paisOrigen = paisOrigen;
    }

    public String toString() {
        return "El producto Refrigerado tiene el código del O.S.A: " + codOSA + ", con fecha de envasado: "
                + new SimpleDateFormat("dd-MM-yyyy").format(fechaEnvasado) + ", siendo la temperatura recomendada para su mantenimiento de: "
                + tempRecom + ", con origen: " + paisOrigen + ".\n";
    }

}
