import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MainThrowsCheckedExceptionV2 {
  public static void main(String[] args) {
    /*
     * if (args.length < 1) {
     * System.out.
     * println("Por favor, pasa un argumento con la ruta a un fichero de texto para leerlo"
     * );
     * return;
     * }
     */
    try {
      leerArchivo(args[0]);
    } catch (FileNotFoundException ex) {
      System.out.println("Excepción 'FileNotFoundException' caputarada desde fuera: Fichero '" +
          args[0] + "' no encontrado");
      return;
    } catch (IOException ex) {
      System.out.println("Excepción 'IOException' caputarada desde fuera");
    } catch (NullPointerException ex) {
      System.out.println("Excepción 'NullPointerException' caputarada desde fuera");
    } catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Excepción 'ArrayIndexOutOfBoundsException' caputarada desde fuera");
    }

    System.out.println("Bye, bye...");
    return;
  }

  public static void leerArchivo(String pathFichero) throws FileNotFoundException, IOException {
    FileReader archivo;
    archivo = new FileReader(pathFichero);
    BufferedReader bufferedReader = new BufferedReader(archivo);
    String line;
    while ((line = bufferedReader.readLine()) != null) {
      System.out.println("line: '" + line + "'");
    }
    archivo.close();
    line = null;
    System.out.println("line.length(): '" + line.length() + "'");
    while ((line = bufferedReader.readLine()) != null) {
      System.out.println("line: '" + line + "'");
    }
  }

}
