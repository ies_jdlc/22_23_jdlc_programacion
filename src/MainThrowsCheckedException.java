import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MainThrowsCheckedException {
  public static void main(String[] args) {
    if (args.length < 1) {
      System.out.println("Por favor, pasa un argumento con la ruta a un fichero de texto para leerlo");
      return;
    }
    try {
      leerArchivo(args[0]);
    } catch (FileNotFoundException ex) {
      System.out.println("Excepción 'checked' caputarada desde fuera: Fichero '" + args[0] + "' no encontrado");
      ex.printStackTrace();
      return;
    }
    return;
  }

  public static void leerArchivo(String pathFichero) throws FileNotFoundException {
    FileReader archivo = new FileReader(pathFichero);
    BufferedReader bufferedReader = new BufferedReader(archivo);
    String line;
    try {
      while ((line = bufferedReader.readLine()) != null) {
        System.out.println("line: '" + line + "'");
      }
      archivo.close();

    } catch (IOException exc) {
      System.out.println("Excepción capturada mientras se lee el archivo 'pathFichero': " + exc);
    }
  }

}
