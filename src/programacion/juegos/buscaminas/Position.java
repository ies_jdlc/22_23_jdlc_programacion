package programacion.juegos.buscaminas;

class Position {
  int row;
  int column;

  public Position(int row, int column) {
    this.row = row;
    this.column = column;
  }

  @Override
  public String toString() {
    return "[" + row + "," + column + "]";
  }  
}
