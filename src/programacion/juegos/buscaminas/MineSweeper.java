package programacion.juegos.buscaminas;

import java.util.ArrayList;
import java.util.List;

import programacion.juegos.exceptions.GameOverException;
import programacion.juegos.exceptions.YouWinException;

public class MineSweeper {
  static final Integer MAX_NUM_ROWS = 1000;
  static final Integer MAX_NUM_COLUMNS = 1000;

  Cell[][] board;
  final Integer numRows;
  final Integer numColumns;
  final int numMines;
  Integer numVisibleCells = 0;
  Integer numFlags = 0;

  public MineSweeper(Integer numRows, Integer numColumns, Integer numMines) {
    if (numRows > MAX_NUM_ROWS) {
      numRows = MAX_NUM_ROWS;
    }
    if (numColumns > MAX_NUM_COLUMNS) {
      numColumns = MAX_NUM_COLUMNS;
    }
    if (numMines > numRows * numColumns) {
      numMines = numRows * numColumns;
    }

    this.numRows = numRows;
    this.numColumns = numColumns;
    this.numMines = numMines;

    board = new Cell[numRows][numColumns];
    for (int row = 0; row < numRows; row++) {
      for (int column = 0; column < numColumns; column++) {
        board[row][column] = new Cell();
      }
    }

    setMines(this.numMines);
  }

  private boolean setMines(int numMines) {
    for (int numMinesSet = 0; numMinesSet < numMines; numMinesSet++) {
      if (!setMineAtRandomPosition()) {
        System.out.println("ERROR: Unable to set mine " + (numMinesSet + 1) + " of " + numMines);
        return false;
      }
    }
    return true;
  }

  boolean setMineAtRandomPosition() {
    while (true) {
      int randomRow = (int) (Math.random() * numRows);
      int randomColumn = (int) (Math.random() * numColumns);
      if (board[randomRow][randomColumn].isMine() == false) {
        board[randomRow][randomColumn].setMine();
        return true;
      }
    }

    /*
     * else {
     * // Set mines at cells with randomly generated positions could take a long
     * time
     * // if percentage of mines per number of cells is high, and probability of
     * // finding a mine is higher than finding an empty cell.
     * // It's better to, once we have hit a cell with a mine, iterate the board
     * until
     * // we find an empty cell instead of using randomly generated positions
     * 
     * for (int rowOffset = 0; rowOffset < numRows; rowOffset++) {
     * for (int columnOffset = 0; columnOffset < numColumns; columnOffset++) {
     * int nextRow = (randomRow + rowOffset) % numRows;
     * int nextColumn = (randomColumn + columnOffset) % numColumns;
     * if (board[nextRow][nextColumn].isMine() == false
     * && board[nextRow][nextColumn].isVisible() == false) {
     * board[nextRow][nextColumn].setMine();
     * return true;
     * }
     * }
     * }
     * }
     */
  }

  public boolean click(int row, int column) throws GameOverException, YouWinException {
    if (row < 0 || row >= numRows || column < 0 || column >= numColumns) {
      // Invalid position
      return false;
    }

    Cell cell = board[row][column];

    if (cell.isVisible()) {
      System.out.println("You already clicked in this cell!");
      return false;
    }

    if (cell.hasFlag()) {
      System.out.println("There is a Flag in this cell!");
      return false;
    }

    if (cell.isMine()) {
      throw new GameOverException();
    }

    cell.setVisible();
    numVisibleCells++;
    int adjacentMines = countAdjacentMines(row, column);
    cell.setAdjacentMinesCount(adjacentMines);

    if (adjacentMines == 0) {
      uncoverCellsWithoutAdjacentMines(row, column);
    }

    // If the sum of visible cells plus the number of mines is equal to the total
    // number of cells, we have won!!
    if ((numVisibleCells + numMines) >= (numRows * numColumns)) {
      throw new YouWinException();
    }

    return true;
  }

  void uncoverCellsWithoutAdjacentMines(int row, int column) {
    if (row < 0 || row >= numRows || column < 0 || column >= numColumns) {
      // Invalid position
      return;
    }

    List<Position> adjacentPositions = getAdjacentPositions(row, column);
    for (Position position : adjacentPositions) {
      Cell cell = board[position.row][position.column];
      if (cell.isVisible() == true) {
        // Recursion break condition
        continue;
      }

      // First thing to do is setting the cell visibility to true to avoid infinite
      // recursion
      cell.setVisible();
      numVisibleCells++;

      int adjacentMines = countAdjacentMines(position.row, position.column);
      cell.setAdjacentMinesCount(adjacentMines);

      // If the count of adjacent mines is 0,
      // we recursively uncover all the adjacent cells
      if (adjacentMines == 0) {
        // Recursive call:
        uncoverCellsWithoutAdjacentMines(position.row, position.column);
      }
    }
  }

  List<Position> getAdjacentPositions(int row, int column) {
    if (row < 0 || row >= numRows || column < 0 || column >= numColumns) {
      // Invalid position
      return new ArrayList<>();
    }

    List<Position> adjacentPositions = new ArrayList<Position>();
    int adjacentRow;
    int adjacentColumn;

    for (int rowOffset = -1; rowOffset <= 1; rowOffset++) {
      for (int columnOffset = -1; columnOffset <= 1; columnOffset++) {
        adjacentRow = row + rowOffset;
        adjacentColumn = column + columnOffset;

        // Check adjacent cell is inside the board
        if (adjacentRow < 0 || adjacentRow >= numRows
            || adjacentColumn < 0 || adjacentColumn >= numColumns) {
          continue;
        }
        // Check adjacent cell is not the original one
        if (rowOffset == 0 && columnOffset == 0) {
          continue;
        }
        // If adjacent position is valid, add it to the list
        adjacentPositions.add(new Position(adjacentRow, adjacentColumn));
      }
    }
    return adjacentPositions;
  }

  int countAdjacentMines(int row, int column) {
    int adjacentMines = 0;
    List<Position> adjacentPositions = getAdjacentPositions(row, column);

    for (Position position : adjacentPositions) {
      // If adjacent cell has a mine, increment the adjacentMines counter
      if (board[position.row][position.column].isMine()) {
        adjacentMines++;
      }
    }
    return adjacentMines;
  }

  @Override
  public String toString() {
    return toString(false);
  }

  public String toString(boolean printAll) {
    String cadena;
    cadena = "===> MineSweeper " + numRows + "x" + numColumns + ", " + numMines
        + " mines, " + numVisibleCells + " visible cells, " + (numRows * numColumns - numVisibleCells - numMines)
        + " remaining cells to win " + (printAll ? " [PRINT ALL]  " : "") + "\n";

    cadena += "   ";
    for (int column = 1; column <= numColumns; column++) {
      cadena += (column % 10);
    }
    cadena += "\n   " + "-".repeat(numColumns) + "\n";
    for (int row = 0; row < numRows; row++) {
      cadena += String.format("%2d|", row + 1);
      for (int column = 0; column < numColumns; column++) {
        Cell cell = board[row][column];
        if (cell.isVisible()) {
          int adjacentMines = cell.getAdjacentMinesCount();
          if (adjacentMines == 0) {
            cadena += " ";
          } else {
            cadena += adjacentMines;
          }
        } else if (cell.hasFlag()) {
          if (printAll && cell.isMine()) {
            cadena += "*";
          } else {
            cadena += "F";
          }
        } else if (printAll) {
          if (cell.isMine()) {
            cadena += "*";
          } else {
            int adjacentMines = cell.getAdjacentMinesCount();
            if (adjacentMines < 0) {
              adjacentMines = countAdjacentMines(row, column);
            }
            cadena += adjacentMines;
          }
        } else {
          cadena += "#";
        }
      }
      cadena += "|\n";
    }
    cadena += "   " + "-".repeat(numColumns) + "\n";
    return cadena;
  }

  public void toggleFlag(int row, int column) {
    if (row < 0 || row >= numRows || column < 0 || column >= numColumns) {
      // Invalid position
      return;
    }

    if (board[row][column].hasFlag()) {
      board[row][column].removeFlag();
      numFlags--;
    } else {
      board[row][column].putFlag();
      numFlags++;
    }
  }

}