package programacion.juegos.buscaminas;

class Cell {
  boolean visible;
  boolean mine;
  int adjacentMinesCount;
  boolean adjacentMinesCounted;
  boolean flag;

  public boolean isVisible() {
    return visible;
  }

  public void setVisible() {
    visible = true;
  }

  // public void setVisible(boolean value) {
  //   visible = value;
  // }

  public boolean isMine() {
    return mine;
  }

  public void setMine() {
    mine = true;
  }

  public boolean hasFlag() {
    return flag;
  }

  public void putFlag() {
    flag = true;
  }

  public void removeFlag() {
    flag = false;
  }

  public void setMine(boolean value) {
    mine = value;
  }

  public int getAdjacentMinesCount() {
    if(adjacentMinesCounted == false) {
      return -1;
    }
    return adjacentMinesCount;
  }

  public void setAdjacentMinesCount(int adjacentMinesCount) {
    this.adjacentMinesCount = adjacentMinesCount;
    this.adjacentMinesCounted = true;
  }
}
