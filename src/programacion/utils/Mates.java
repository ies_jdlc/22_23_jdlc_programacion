package programacion.utils;

import java.util.HashMap;

public class Mates {

  /*
   * Métodos para calcular el factorial de un número usando recursividad. El
   * método está sobrecargado para recibir y devolver int y long.
   */
  public static int factorial(int value) {
    if (value == 0) {
      return 1;
    }
    int factorialMinus1 = factorial(value - 1);
    int result = value * factorialMinus1;
    return result;
  }

  public static long factorial(long value) {
    if (value == 0) {
      return 1;
    }
    long factorialMinus1 = factorial(value - 1);
    long result = value * factorialMinus1;
    return result;
  }

  /*
   * Métodos para calcular el número de la sucesión de Fibonacci de una posición
   * concreta.
   * 
   * Tenemos un contador fibonacciCallDepth que se incrementa cada vez que
   * entramos a ejecutar el método fibonacci y se decrementa antes de devolver el
   * valor, de modo que tenemos la profundidad de la recursión en cada momento.
   * 
   * Para optimizar, hemos creado un HashMap que utilizaremos como cache, para no
   * recalcular el número de una posición ya calculada.
   * 
   * El método fibonacci, además de la posición a calcular, recibe 2 boolean para
   * activar el modo verbose y el uso de la cache
   */

  private static HashMap<Integer, Long> fibonacciCache = new HashMap<Integer, Long>();
  private static int fibonacciCallDepth = 0;

  public static long fibonacci(int pos) {
    return fibonacci(pos, false, false);
  }

  public static long fibonacci(int pos, boolean verbose) {
    return fibonacci(pos, verbose, false);
  }

  public static long fibonacciCached(int pos, boolean verbose) {
    return fibonacci(pos, verbose, true);
  }

  public static long fibonacci(int pos, boolean verbose, boolean useCache) {
    fibonacciCallDepth++;
    Long fibonacciValue = null;

    if (verbose) {
      System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
          + " fibonacci(" + pos + ") IN ->");
    }

    if (useCache) {
      // Look up pos in cache
      fibonacciValue = fibonacciCache.get(pos);
      if (fibonacciValue != null) {
        if (verbose) {
          System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
              + " fibonacci(" + pos + ") " + "returns " + fibonacciValue + " (HIT) OUT <-");
        }
        fibonacciCallDepth--;
        return fibonacciValue;
      }
      // If fibonacciValue == null, position pos is not cached, we must continue the
      // execution flow to compute it
    }

    if (pos == 0) {
      fibonacciValue = 0L;
    } else if (pos == 1) {
      fibonacciValue = 1L;
    }

    if (fibonacciValue != null) {
      if (useCache) {
        // Insert value in fibonacciCache
        Long rc = fibonacciCache.put(pos, fibonacciValue);
        if (rc != null) {
          System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
              + " WARNING!! Already inserted pair <" + pos + "," + fibonacciValue + ">");
        } else {
          if (verbose) {
            System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
                + " fibonacci(" + pos + ")=" + fibonacciValue + " (ADDED) (recursion break condition) OUT <-");
          }
        }
      } else if (verbose) {
        System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
            + " fibonacci(" + pos + ") " + "returns " + fibonacciValue + " (recursion break condition) OUT <-");
      }
      fibonacciCallDepth--;
      return fibonacciValue;
    }

    long fibonacciMinusOne = fibonacci(pos - 1, verbose, useCache);
    long fibonacciMinusTwo = fibonacci(pos - 2, verbose, useCache);
    fibonacciValue = fibonacciMinusOne + fibonacciMinusTwo;

    if (useCache) {
      // Insert value in fibonacciCache
      Long rc = fibonacciCache.put(pos, fibonacciValue);
      if (rc != null) {
        System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
            + " WARNING!! Already inserted pair <" + pos + "," + fibonacciValue + ">");
      } else {
        if (verbose) {
          System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
              + " fibonacci(" + pos + ")=" + fibonacciValue + " (ADDED) OUT <-");
        }
      }
    } else if (verbose) {
      System.out.println("-".repeat(fibonacciCallDepth) + "[fib(" + (useCache ? "cache" : "") + ")]"
          + " fibonacci(" + pos + ")" + " returns " + fibonacciValue + " OUT <-");
    }
    fibonacciCallDepth--;
    return fibonacciValue;
  }
}
