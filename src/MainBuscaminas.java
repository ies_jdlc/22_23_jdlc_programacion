import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import programacion.juegos.buscaminas.MineSweeper;
import programacion.juegos.exceptions.GameOverException;
import programacion.juegos.exceptions.YouWinException;

public class MainBuscaminas {
  public static void main(String[] args) {
    final int MIN_ROWS = 3;
    final int MAX_ROWS = 100;
    final int MIN_COLUMNS = 3;
    final int MAX_COLUMNS = 100;
    final int MIN_MINES = 1;
    final int MAX_MINES = MAX_ROWS * MAX_COLUMNS;

    Integer num_rows = null;
    Integer num_columns = null;
    Integer num_mines = null;

    // Process provided arguments
    if (args.length == 0) {
      printSyntax();
      return;
    }

    for (String argument : args) {
      if (argument.equals("-h") || argument.equals("--help")) {
        printSyntax();
        return;
      } else if (num_rows == null) {
        try {
          num_rows = Integer.parseInt(argument);
        } catch (Exception e) {
          System.out.println("Exception when reading num_rows. Invalid format '" + argument
              + "'. Exception: " + e.getMessage());
          return;
        }
      } else if (num_columns == null) {
        try {
          num_columns = Integer.parseInt(argument);
        } catch (Exception e) {
          System.out.println("Exception when reading num_columns. Invalid format '" + argument
              + "'. Exception: " + e.getMessage());
          return;
        }
      } else if (num_mines == null) {
        try {
          num_mines = Integer.parseInt(argument);
        } catch (Exception e) {
          System.out.println("Exception when reading num_mines. Invalid format '" + argument
              + "'. Exception: " + e.getMessage());
          return;
        }
      } else {
        System.out.println("Unexpected argument: '" + argument + "'. Ignoring it...");
      }
    }

    // Check provided values are valid
    if (num_rows == null) {
      System.out.println("Error: Number of rows not provided");
      return;
    } else if (num_rows < MIN_ROWS || num_rows > MAX_ROWS) {
      System.out.println("Error: Invalid number of rows: " + num_rows);
      return;
    }

    if (num_columns == null) {
      System.out.println("Error: Number of columns not provided");
      return;
    } else if (num_columns < MIN_COLUMNS || num_columns > MAX_COLUMNS) {
      System.out.println("Error: Invalid number of columns: " + num_columns);
      return;
    }

    if (num_mines == null) {
      System.out.println("Error: Number of mines not provided");
      return;
    } else if (num_mines < MIN_MINES || num_mines > MAX_MINES) {
      System.out.println("Error: Invalid number of mines: " + num_mines);
      return;
    }

    // So far, so good. Let's create the game and start playing...
    MineSweeper buscaminas = new MineSweeper(num_rows, num_columns, num_mines);
    
    Scanner scannerIn = new Scanner(System.in);
    try {
      do {
        System.out.println(buscaminas);
        System.out.print("\nEnter row and column numbers separated by spaces, prepend an F to toggle a flag >> ");

        String line = scannerIn.nextLine();
        boolean toggleFlag = false;

        // Toggle Flag: Check if first character is F indicating to toggle the flag
        if (line.length() >= 2 && Character.toUpperCase(line.charAt(0)) == 'F' && line.charAt(1) == ' ') {
          System.out.println("ToggleFlag = true!");
          toggleFlag = true;
          // Remove first character from line
          line = line.substring(1);
          line.trim();
        }

        // Scan line to extract expected row number and column number separated by
        // spaces
        Scanner lineParser = new Scanner(line);
        int row = -1;
        int column = -1;

        try {
          row = lineParser.nextInt();
          row--;
        } catch (InputMismatchException exc) {
          System.out.println("Invalid format for row number");
          lineParser.close();
          continue;
        } catch (NoSuchElementException exc) {
          System.out.println("Missing value for row");
          lineParser.close();
          continue;
        }

        try {
          column = lineParser.nextInt();
          column--;
        } catch (InputMismatchException exc) {
          System.out.println("Invalid format for column number");
          lineParser.close();
          continue;
        } catch (NoSuchElementException exc) {
          System.out.println("Missing value for column");
          lineParser.close();
          continue;
        }

        lineParser.close();

        // Validate read row and column values
        if (row < 0 || row >= num_rows) {
          System.out.println("Invalid row [1," + num_rows + "]: " + (row + 1));
          continue;
        }
        if (column < 0 || column >= num_columns) {
          System.out.println("Invalid column [1," + num_columns + "]: " + (column + 1));
          continue;
        }

        if (toggleFlag) {
          // If row and column numbers were prepended by an F character, we should toggle
          // the flag in that position
          buscaminas.toggleFlag(row, column);
        } else {
          // Otherwise, click on the position
          buscaminas.click(row, column);
        }
      } while (true);
    } catch (GameOverException exc) {
      System.out.println("=".repeat(31) + "\n"
          + "=".repeat(10) + " GAME OVER " + "=".repeat(10)
          + "\n" + "=".repeat(31));
    } catch (YouWinException exc) {
      System.out.println("=".repeat(50) + "\n"
          + "=".repeat(10) + " YOU WIN!! CONGRATULATIONS!!! " + "=".repeat(10)
          + "\n" + "=".repeat(50));
    } finally {
      scannerIn.close();
    }
    System.out.println(buscaminas.toString(true));
    return;
  }

  private static void printSyntax() {
    System.out.println("""
        java MainBuscaminas [-h|--help] num_rows num_columns num_mines
          -h or --help: print this help message
          num_rows: number of rows of the board
          num_columns: number of columns of the board
          num_mines: number of mines in the board

        """);
  }
}
