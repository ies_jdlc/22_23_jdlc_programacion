public class MainEjemplosSplitString {
  public static void main(String[] args) {
    final String CADENA_EJEMPLO_1 = "Hola, que tal";
    final String REGEX_DNI = "[0-9]{1,8}[A-Za-z]";

    String[] arrayCadenas = CADENA_EJEMPLO_1.split("[, ]");
    for (int i = 0; i < arrayCadenas.length; i++) {
      System.out.println("arrayCadenas[" + i + "]='" + arrayCadenas[i] + "'");
    }

    String dni;

    dni = "123P";
    System.out.println("dni.matches(REGEX_DNI)=" + dni.matches(REGEX_DNI) + " [" + dni + "]");

    dni = "12345J6789";
    System.out.println("dni.matches(REGEX_DNI)=" + dni.matches(REGEX_DNI) + " [" + dni + "]");

    dni = "12345678A";
    System.out.println("dni.matches(REGEX_DNI)=" + dni.matches(REGEX_DNI) + " [" + dni + "]");

    dni = "123456789A";
    System.out.println("dni.matches(REGEX_DNI)=" + dni.matches(REGEX_DNI) + " [" + dni + "]");

    dni = "12345678";
    System.out.println("dni.matches(REGEX_DNI)=" + dni.matches(REGEX_DNI) + " [" + dni + "]");

    dni = "1234567AA";
    System.out.println("dni.matches(REGEX_DNI)=" + dni.matches(REGEX_DNI) + " [" + dni + "]");


  }

}
