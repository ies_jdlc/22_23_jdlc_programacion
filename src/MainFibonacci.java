import java.util.InputMismatchException;
import java.util.Scanner;

import programacion.utils.Mates;

/*
 You can check the results with this online fibonacci calculator:
 https://es.numberempire.com/fibonaccinumbers.php
*/
public class MainFibonacci {
  final static int MIN_POS = 0;
  final static int MAX_POS = 92; // Posición máxima que cabe en un long

  public static void main(String[] args) {
    Integer posicionFibonacci = null;
    boolean verbose = false;
    boolean useCache = false;

    for (String argument : args) {
      if (argument.equals("-h") || argument.equals("--help")) {
        printSyntax();
        return;
      } else if (argument.equals("-v") || argument.equals("--verbose")) {
        verbose = true;
      } else if (argument.equals("-c") || argument.equals("--cache")) {
        useCache = true;
      } else if (posicionFibonacci == null) {
        try {
          posicionFibonacci = Integer.parseInt(argument);
        } catch (Exception e) {
          System.out.println("[MainFibonacci] Excepción capturada: " + e.getMessage() + ". El argumento '" + argument
              + "' no tiene formato de Integer");
        }
      } else {
        System.out.println("[MainFibonacci] Argumento no esperado: '" + argument + "'. Lo ignoramos...");
      }
    }
    if (verbose) {
      System.out.println("[MainFibonacci] Verbose output enabled");
    }
    if (useCache) {
      System.out.println("[MainFibonacci] cache activated");
    }

    if (posicionFibonacci == null || posicionFibonacci < MIN_POS || posicionFibonacci > MAX_POS) {
      Scanner scan = new Scanner(System.in);
      do {
        if (posicionFibonacci != null) {
          System.out.print("[MainFibonacci] Posición fibonacci inválida."
              + " Introduce la posición deseada "
              + "(mínimo " + MIN_POS + ", máximo " + MAX_POS + "): ");
        } else {
          System.out.print("[MainFibonacci] Introduce la posición deseada "
              + "(mínimo " + MIN_POS + ", máximo " + MAX_POS + "): ");
        }
        try {
          posicionFibonacci = scan.nextInt();
        } catch (InputMismatchException exc) {
          System.out.println("[MainFibonacci] Excepción: Se esperaba la posición en formato numérico entre " + MIN_POS
              + " y " + MAX_POS + ". Se ha recibido: '" + scan.nextLine() + "'");
        }
      } while (posicionFibonacci == null || posicionFibonacci < MIN_POS || posicionFibonacci > MAX_POS);
      scan.close(); // Liberar recursos Scanner
    }

    System.out.println(
        "===> [main] fibonacci(" + posicionFibonacci + ", verbose=" + verbose + ", useCache=" + useCache + ")");
    System.out.println("<=== [main] fibonacci(" + posicionFibonacci + ", verbose=" + verbose + ", useCache=" + useCache
        + ") = " + Mates.fibonacci(posicionFibonacci, verbose, useCache));

  }

  private static void printSyntax() {
    System.out.println("""
        java MainFibonacci [-h|--help] [-c|--cache] [-v|--verbose] position
          -h or --help: print this help message.
          -c or --cache: activate cache.
          -v or --verbose: enable verbose output.
          position: Fibonacci position to compute.
        """);
  }
}
