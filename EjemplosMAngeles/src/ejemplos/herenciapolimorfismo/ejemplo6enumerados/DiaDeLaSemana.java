package ejemplos.herenciapolimorfismo.ejemplo6enumerados;

public enum DiaDeLaSemana {
  LUNES, MARTES, MIÉRCOLES, JUEVES, VIERNES, SÁBADO, DOMINGO
}