package ejemplos.herenciapolimorfismo.ejemplo4clasesabstract;

public abstract class ClaseAbstracta1 {
  String nombrePersona;

  public void saludo() {
    System.out.println("Hola Mundo");
  }

  public abstract void saludar();

}
