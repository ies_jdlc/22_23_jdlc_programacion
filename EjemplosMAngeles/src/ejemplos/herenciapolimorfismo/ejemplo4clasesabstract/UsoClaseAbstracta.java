package ejemplos.herenciapolimorfismo.ejemplo4clasesabstract;

public class UsoClaseAbstracta {
  public static void main(String[] args) {
    ClaseAbstracta1 objAbs1 = new Derivada("Pedro");

    // No podemos llamar al constructor ClaseAbstracta1 porque es abstract:
    // ClaseAbstracta1 objAbs2 = new ClaseAbstracta1();

    objAbs1.saludo();
    objAbs1.saludar();
  }

}
