public class MainCuadradoMagico {
  public static void main(String[] args) {
    int[][] cuadradoMagico = {{4, 9, 2}, {3,5,7}, {8,1,6}};
    int[][] cuadradoNoMagico = {{4, 9, 2}, {3,5,7}, {8,1,7}};

    System.out.println("cuadradoMagico "+ (comprobarCuadradoMagico(cuadradoMagico)?"es":"no es") + " mágico");
    System.out.println("cuadradoNoMagico "+ (comprobarCuadradoMagico(cuadradoNoMagico)?"es":"no es") + " mágico");

  }

  public static boolean comprobarCuadradoMagico(int[][] cuadrado) {
    Integer primeraSuma = null;
    Integer acumuladorPorFilas = 0;
    Integer acumuladorPorColumnas = 0;

    for (int i = 0; i < cuadrado.length; i++) {
      acumuladorPorFilas = 0;
      acumuladorPorColumnas = 0;
      for (int j = 0; j < cuadrado.length; j++) {
        acumuladorPorFilas += cuadrado[i][j];
        acumuladorPorColumnas += cuadrado[j][i];
      }
      if(primeraSuma == null){
        primeraSuma = acumuladorPorFilas;
      }

      if(primeraSuma!=acumuladorPorFilas || primeraSuma!= acumuladorPorColumnas){
        return false;
      }
    }
    return true;

  }

}
