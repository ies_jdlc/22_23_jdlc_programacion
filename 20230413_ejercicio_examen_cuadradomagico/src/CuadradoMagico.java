public class CuadradoMagico {
  public static boolean comprobarCuadradoMagico(int[][] cuadrado) {
    Integer primeraSuma = null;
    Integer acumuladorPorFilas = 0;
    Integer acumuladorPorColumnas = 0;

    for (int i = 0; i < cuadrado.length; i++) {
      acumuladorPorFilas = 0;
      acumuladorPorColumnas = 0;
      for (int j = 0; j < cuadrado.length; j++) {
        acumuladorPorFilas += cuadrado[i][j];
        acumuladorPorColumnas += cuadrado[j][i];
      }
      if (primeraSuma == null) {
        primeraSuma = acumuladorPorFilas;
      }

      if (primeraSuma != acumuladorPorFilas || primeraSuma != acumuladorPorColumnas) {
        return false;
      }
    }
    return true;
  }

}
