import java.util.List;
import java.util.ArrayList;
import clases.*;

public class SeresVivosMain {
    public static void main(String[] args) {
        List<SerVivo> listaSeresVivos = new ArrayList<>();
        Perro milu = new Perro("milu");
        Perro jesus = new Perro("jesus");
        Cactus jacinto = new Cactus("jacinto");
        Cactus cactus2 = new Cactus("npc");
        listaSeresVivos.add(jesus);
        listaSeresVivos.add(milu);
        listaSeresVivos.add(jacinto);
        listaSeresVivos.add(cactus2);
        for (SerVivo contenido : listaSeresVivos) {
            System.out.println("==>" + contenido);
            contenido.nacer();
            contenido.crecer();
            contenido.reproducir();
            // contenido.desplazarse(); No se podrá usar porque no forma parte de SerVivo
            contenido.morir();
        }

    }
}