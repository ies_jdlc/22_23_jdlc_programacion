package clases;

public class Perro extends AbstractAnimal {
    private String nombre;

    public Perro(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void desplazarse() {
        System.out.println("Soy un perro y dispongo de un movimiento cuadrupedo");
    }

    @Override
    public String toString() {
        return super.toString() + "y soy un perro y me llamo " + nombre;
    }

}
