package clases;

public abstract class AbstractPlanta implements SerVivo {
    protected String nombre;
    protected String especie;

    public void nacer() {
        System.out.println("Soy una planta y estoy brotando");
    }

    public void crecer() {
        System.out.println("Soy una planta y crezco muy despacito");
    }

    public void reproducir() {
        System.out.println("Soy una planta y quiero tener plantitas");
    }

    public void morir() {
        System.out.println("Soy una planta y me ha comido un zombi, muero");
    }

    public abstract void florecer();

    @Override
    public String toString() {
        return "Soy una planta ";
    }
}
