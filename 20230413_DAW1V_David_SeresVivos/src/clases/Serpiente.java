package clases;

public class Serpiente extends AbstractAnimal {
    private String nombre;

    public Serpiente(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void desplazarse() {
        System.out.println("Soy una serpiente y mi movimiento es reptar");
    }

    @Override
    public String toString() {
        return super.toString() + "y soy una serpiente y me llamo " + nombre;
    }
}
