package clases;

public abstract class AbstractAnimal implements SerVivo {
    protected String nombre;
    protected String especie;

    public void nacer() {
        System.out.println("Soy un animal y estoy naciendo");
    }

    public void crecer() {
        System.out.println("Soy un animal y crezco muy despacito");
    }

    public void reproducir() {
        System.out.println("Soy un animal y quiero tener animalitos");
    }

    public void morir() {
        System.out.println("Soy un animal y he cumplido mi cliclo de vida, muero");
    }

    public abstract void desplazarse();

    @Override
    public String toString() {
        return "Soy un animal ";
    }
}
