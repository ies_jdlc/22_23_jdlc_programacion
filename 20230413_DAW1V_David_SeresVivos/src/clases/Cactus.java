package clases;

public class Cactus extends AbstractPlanta {
    private String nombre;

    public Cactus(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void florecer() {
        System.out.println("Soy un cactus y mira que bellas mis flores");
    }

    @Override
    public String toString() {
        return super.toString() + "y soy un cactus y me llamo " + nombre;
    }
}
