package clases;

public class Geranio extends AbstractPlanta {
    private String nombre;

    public Geranio(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void florecer() {
        System.out.println("Soy un geranio y mira que bello florezco");
    }

    @Override
    public String toString() {
        return super.toString() + "y soy un geranio y me llamo " + nombre;
    }
}
