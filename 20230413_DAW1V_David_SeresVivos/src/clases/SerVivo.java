package clases;

public interface SerVivo {

    int MAXIMO_SERES_VIVOS = 30;

    void nacer();

    void crecer();

    void reproducir();

    void morir();

}